unit UClientes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Mask, Grids, ExtCtrls, ComCtrls,SQLiteTable3,SqlTabGrid,
  UDbMod;

type
  TTClientes = class(TForm)
    PageControl1: TPageControl;
    Inserir: TTabSheet;
    Splitter1: TSplitter;
    Panel1: TPanel;
    GridInserir: TStringGrid;
    ScrollBox1: TScrollBox;
    Panel3: TPanel;
    No: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EdtNome: TEdit;
    GroupBox1: TGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label5: TLabel;
    EdtLog: TEdit;
    EdtNumero: TEdit;
    EdtBairro: TEdit;
    EdtCEP: TMaskEdit;
    EdtCidade: TEdit;
    CmbEstado: TComboBox;
    EdtComplemento: TEdit;
    EdtTelefone: TMaskEdit;
    EdtEmail: TEdit;
    BtnInserir: TBitBtn;
    BtnCancelar: TBitBtn;
    Editar: TTabSheet;
    Splitter2: TSplitter;
    Panel2: TPanel;
    GridAtualizar: TStringGrid;
    ScrollBox2: TScrollBox;
    Label4: TLabel;
    Label8: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    EdtNome1: TEdit;
    EdtCodigo: TEdit;
    EdtTelefone1: TMaskEdit;
    EdtEmail1: TEdit;
    BtnAtualizar: TBitBtn;
    BtnCancelar1: TBitBtn;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    EdtLog1: TEdit;
    EdtNumero1: TEdit;
    EdtBairro1: TEdit;
    EdtCEP1: TMaskEdit;
    EdtCidade1: TEdit;
    CmbEstado1: TComboBox;
    EdtComplemento1: TEdit;
    BtnExcluir: TBitBtn;
    Label21: TLabel;
    EdtCPF: TMaskEdit;
    Label23: TLabel;
    EdtCPF1: TMaskEdit;
    Label22: TLabel;
    EdtDataN: TMaskEdit;
    Label24: TLabel;
    EdtDataN1: TMaskEdit;
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnExcluirClick(Sender: TObject);
    procedure BtnAtualizarClick(Sender: TObject);
    procedure BtnInserirClick(Sender: TObject);
    procedure EditarShow(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GridAtualizarSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure InserirShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TClientes: TTClientes;

implementation
var
   T:TSQLiteUniTable;
   SQL:string;

{$R *.dfm}

procedure TTClientes.BtnExcluirClick(Sender: TObject);
var
   confirma:integer;
begin
   confirma:=MessageDlg('Deseja excluir o registro selecionado?',mtWarning,mbYesNo,0);
   if confirma=6 then begin
      SQL:='DELETE FROM ENDERECO WHERE ';
      SQL:=SQL+'EN=(SELECT END FROM CLIENTE WHERE CN="'+EdtCodigo.Text+'")';
    Try
      B.execSQL(SQL);
    Except
      on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
    End;

    SQL:='DELETE FROM CLIENTE WHERE CN="'+EdtCodigo.Text+'"';
    Try
       B.execSQL(SQL);
    Except
       on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
    End;

    ShowMessage('Cliente exclu�do com sucesso!');
    SQL:='SELECT C.CN,C.CNOME AS NOME,C.DATAN,C.CPF,C.EMAIL,C.TEL AS TEL,C.END,E.RUA,E.NUMERO,E.COMPLEMENTO, ';
    SQL:=SQL+' E.BAIRRO,E.CIDADE,E.ESTADO,E.CEP ';
    SQL:=SQL+' FROM CLIENTE C JOIN ENDERECO E WHERE C.END=E.EN'  ;
    T:=B.GetUniTable(SQL);
    ExecTableToGrid(T,GridAtualizar);
    GridAtualizar.FixedRows:=1
  end;
end;

procedure TTClientes.BtnInserirClick(Sender: TObject);
var
  cep,tel,cpf,dataN:string;
  codend:integer;
begin

  if EdtCEP.EditText='_____-___' then  cep:=''
  else cep:=EdtCEP.Text;
  if EdtTelefone.EditText='(__)____-____' then tel:=''
  else tel:= EdtTelefone.EditText;
  if EdtCPF.EditText='___.___.___-__' then  cpf:=''
  else cpf:=EdtCPF.Text;
  if EdtDataN.EditText='__/__/____' then  dataN:=''
  else dataN:=EdtDataN.Text;

  SQL:='INSERT INTO ENDERECO VALUES(NULL,"'+EdtLog.Text+'","'+EdtNumero.Text+'","'+EdtComplemento.Text+'","'+EdtBairro.Text+'", ';
  SQL:=SQL+'"'+EdtCidade.Text+'","'+CmbEstado.Text+'","'+cep+'")';
  Try
       B.execSQL(SQL);
  Except
       on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
  End;

  SQL:='SELECT last_insert_rowid()';
  T:=B.GetUniTable(SQL);
  codend:=T.FieldAsInteger(0);

  SQL:='INSERT INTO CLIENTE VALUES (null, "'+EdtNome.Text+'","'+dataN+'", ' ;
  SQL:=SQL+ '"'+cpf+'","'+EdtEmail.Text+'","'+tel+'","'+IntToStr(codend)+'");';
  Try
     B.execSQL(SQL);
  Except
     on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
  End;
  showmessage('Cliente cadastrado com sucesso!');

  SQL:='SELECT C.CNOME AS NOME,C.DATAN,C.CPF,C.EMAIL,C.TEL AS TEL,E.RUA,E.NUMERO,E.COMPLEMENTO, ';
  SQL:=SQL+' E.BAIRRO,E.CIDADE,E.ESTADO,E.CEP ';
  SQL:=SQL+' FROM CLIENTE C JOIN ENDERECO E WHERE C.END=E.EN'  ;
  T:=B.GetUniTable(SQL);
  ExecTableToGrid(T,GridInserir);
  GridInserir.FixedRows:=1;
  BtnCancelarClick(Sender)
end;

procedure TTClientes.EditarShow(Sender: TObject);
begin
  SQL:='SELECT C.CN,C.CNOME AS NOME,C.DATAN,C.CPF,C.EMAIL,C.TEL AS TEL,C.END,E.RUA,E.NUMERO,E.COMPLEMENTO, ';
  SQL:=SQL+' E.BAIRRO,E.CIDADE,E.ESTADO,E.CEP ';
  SQL:=SQL+' FROM CLIENTE C JOIN ENDERECO E WHERE C.END=E.EN'  ;
  T:=B.GetUniTable(SQL);
  ExecTableToGrid(T,GridAtualizar);
  GridAtualizar.FixedRows:=1;
  T.Free
end;

procedure TTClientes.FormShow(Sender: TObject);
begin
   PageControl1.ActivePageIndex:=0;
end;

procedure TTClientes.GridAtualizarSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
if Arow =0 then  exit;
   EdtCodigo.Text:=GridAtualizar.Cells[0,Arow];
   EdtNome1.Text:= GridAtualizar.Cells[1,Arow];
   EdtDataN1.Text:= GridAtualizar.Cells[2,Arow];
   EdtCPF1.Text:=  GridAtualizar.Cells[3,Arow] ;
   EdtEmail1.Text:= GridAtualizar.Cells[4,Arow] ;
   EdtTelefone1.Text:= GridAtualizar.Cells[5,Arow] ; 
   EdtLog1.Text:= GridAtualizar.Cells[7,Arow];
   EdtNumero1.Text:=GridAtualizar.Cells[8,Arow];
   EdtComplemento1.Text:=GridAtualizar.Cells[9,Arow];
   EdtBairro1.Text:=GridAtualizar.Cells[10,Arow];
   EdtCidade1.Text:=GridAtualizar.Cells[11,Arow];
   CmbEstado1.Text:=GridAtualizar.Cells[12,Arow];
   EdtCEP1.Text:=GridAtualizar.Cells[13,Arow];
end;

procedure TTClientes.InserirShow(Sender: TObject);
begin
  SQL:='SELECT C.CNOME AS NOME,C.DATAN,C.CPF,C.EMAIL,C.TEL AS TEL,E.RUA,E.NUMERO,E.COMPLEMENTO, ';
  SQL:=SQL+' E.BAIRRO,E.CIDADE,E.ESTADO,E.CEP ';
  SQL:=SQL+' FROM CLIENTE C JOIN ENDERECO E WHERE C.END=E.EN'  ;
  T:=B.GetUniTable(SQL);
  ExecTableToGrid(T,GridInserir);
  GridInserir.FixedRows:=1;
  T.Free
end;

procedure TTClientes.BtnAtualizarClick(Sender: TObject);
var
   cep,tel,cpf,dataN:string;
begin
  if EdtCEP1.EditText='_____-___' then  cep:=''
  else cep:=EdtCEP1.Text;
  if EdtTelefone1.EditText='(__)____-____' then tel:=''
  else tel:= EdtTelefone1.EditText;
  if EdtCPF1.EditText='___.___.___-__' then  cpf:=''
  else cpf:=EdtCPF1.Text;
  if EdtDataN1.EditText='__/__/____' then  dataN:=''
  else dataN:=EdtDataN1.Text;

  SQL:='UPDATE ENDERECO SET RUA="'+EdtLog1.Text+'", NUMERO="'+EdtNumero1.Text+'",COMPLEMENTO="'+EdtComplemento1.Text+'",';
  SQL:=SQL+' CIDADE="'+EdtCidade1.Text+'",ESTADO="'+CmbEstado1.Text+'",CEP="'+cep+'"';
  SQL:=SQL+ ' WHERE EN=(SELECT END FROM CLIENTE WHERE CN="'+EdtCodigo.Text+'")';
  Try
     B.execSQL(SQL);
  Except
     on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
  End;

  SQL:='UPDATE CLIENTE SET CNOME="'+EdtNome1.Text+'", TEL="'+EdtTelefone1.Text+'",';
  SQL:=SQL+' EMAIL="'+EdtEmail1.Text+'",CPF="'+cpf+'",DATAN="'+dataN+'" '   ;
  SQL:=SQL+' WHERE CN="'+EdtCodigo.Text+'"'  ;
  Try
     B.execSQL(SQL);
  Except
     on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
  End;

  ShowMessage('Fornecedor atualizado com sucesso!') ;

  SQL:='SELECT C.CN,C.CNOME AS NOME,C.DATAN,C.CPF,C.EMAIL,C.TEL AS TEL,C.END,E.RUA,E.NUMERO,E.COMPLEMENTO, ';
  SQL:=SQL+' E.BAIRRO,E.CIDADE,E.ESTADO,E.CEP ';
  SQL:=SQL+' FROM CLIENTE C JOIN ENDERECO E WHERE C.END=E.EN'  ;
  T:=B.GetUniTable(SQL);
  ExecTableToGrid(T,GridAtualizar);   
  GridAtualizar.FixedRows:=1
end;

procedure TTClientes.BtnCancelarClick(Sender: TObject);
begin
     EdtNome.Clear;
     EdtLog.Clear;
     EdtNumero.Clear;
     EdtBairro.Clear;
     EdtCidade.Clear;
     EdtCEP.Clear;
     EdtTelefone.Clear;
     EdtEmail.Clear;
     CmbEstado.Text:='';
     EdtComplemento.Clear;
     EdtCPF.Clear;
     EdtDataN.Clear;
     EdtNome1.Clear;
     EdtLog1.Clear;
     EdtNumero1.Clear;
     EdtBairro1.Clear;
     EdtCidade1.Clear;
     EdtCEP1.Clear;
     EdtTelefone1.Clear;
     EdtEmail1.Clear;
     CmbEstado1.Text:='';
     EdtCodigo.Clear;
     EdtComplemento1.Clear;
     EdtCPF1.Clear;
     EdtDataN1.Clear;
end;

end.
