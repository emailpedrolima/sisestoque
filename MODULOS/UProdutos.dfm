object TProdutos: TTProdutos
  Left = 0
  Top = 0
  Caption = 'Estoque'
  ClientHeight = 560
  ClientWidth = 542
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = cmbFornecedorChange
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 542
    Height = 560
    ActivePage = Editar
    Align = alClient
    TabOrder = 0
    ExplicitLeft = 1
    ExplicitTop = 5
    ExplicitWidth = 602
    ExplicitHeight = 572
    object Inserir: TTabSheet
      Caption = 'Inserir'
      OnShow = InserirShow
      ExplicitLeft = 7
      ExplicitTop = 40
      ExplicitWidth = 594
      ExplicitHeight = 544
      object Splitter1: TSplitter
        Left = 0
        Top = 353
        Width = 534
        Height = 3
        Cursor = crVSplit
        Align = alTop
        ExplicitTop = 281
        ExplicitWidth = 263
      end
      object GridInserir: TStringGrid
        Left = 0
        Top = 356
        Width = 534
        Height = 176
        Align = alClient
        ColCount = 7
        FixedCols = 0
        TabOrder = 0
        ExplicitLeft = 16
        ExplicitTop = 387
        ExplicitWidth = 561
        ExplicitHeight = 181
      end
      object ScrollBox1: TScrollBox
        Left = 0
        Top = 0
        Width = 534
        Height = 353
        HorzScrollBar.Range = 300
        VertScrollBar.Range = 150
        Align = alTop
        AutoScroll = False
        TabOrder = 1
        ExplicitLeft = -3
        ExplicitTop = -3
        object Label1: TLabel
          Left = 13
          Top = 32
          Width = 90
          Height = 13
          Caption = 'Nome do produto :'
        end
        object Label2: TLabel
          Left = 13
          Top = 168
          Width = 53
          Height = 13
          Caption = 'Descri'#231#227'o :'
        end
        object Label4: TLabel
          Left = 13
          Top = 97
          Width = 99
          Height = 13
          Caption = 'Pre'#231'o de custo (R$):'
        end
        object Label5: TLabel
          Left = 241
          Top = 96
          Width = 103
          Height = 13
          Caption = 'Pre'#231'o de venda (R$):'
        end
        object Label6: TLabel
          Left = 241
          Top = 64
          Width = 119
          Height = 13
          Caption = 'Quantidade em estoque:'
        end
        object Label7: TLabel
          Left = 13
          Top = 64
          Width = 51
          Height = 13
          Caption = 'Categoria:'
        end
        object Label8: TLabel
          Left = 327
          Top = 32
          Width = 59
          Height = 13
          Caption = 'Fornecedor:'
        end
        object Label3: TLabel
          Left = 13
          Top = 128
          Width = 78
          Height = 13
          Caption = 'Estoque m'#237'nimo:'
        end
        object Foto: TImage
          Left = 303
          Top = 128
          Width = 202
          Height = 145
          Stretch = True
        end
        object Label11: TLabel
          Left = 257
          Top = 128
          Width = 26
          Height = 13
          Caption = 'Foto:'
        end
        object EdtNome: TEdit
          Left = 109
          Top = 29
          Width = 193
          Height = 21
          TabOrder = 0
        end
        object EdtPCusto: TEdit
          Left = 117
          Top = 93
          Width = 100
          Height = 21
          TabOrder = 1
        end
        object EdtPVenda: TEdit
          Left = 347
          Top = 93
          Width = 100
          Height = 21
          TabOrder = 2
        end
        object EdtQntd: TEdit
          Left = 367
          Top = 61
          Width = 100
          Height = 21
          TabOrder = 3
        end
        object EdtCategoria: TEdit
          Left = 77
          Top = 61
          Width = 142
          Height = 21
          TabOrder = 4
        end
        object cmbFornecedor: TComboBox
          Left = 392
          Top = 29
          Width = 121
          Height = 21
          ItemHeight = 13
          TabOrder = 5
          OnChange = cmbFornecedorChange
        end
        object BitConfirmar: TBitBtn
          Left = 83
          Top = 305
          Width = 128
          Height = 25
          Caption = 'Inserir'
          Default = True
          TabOrder = 6
          OnClick = BitConfirmarClick
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333333333333330000333333333333333333333333F33333333333
            00003333344333333333333333388F3333333333000033334224333333333333
            338338F3333333330000333422224333333333333833338F3333333300003342
            222224333333333383333338F3333333000034222A22224333333338F338F333
            8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
            33333338F83338F338F33333000033A33333A222433333338333338F338F3333
            0000333333333A222433333333333338F338F33300003333333333A222433333
            333333338F338F33000033333333333A222433333333333338F338F300003333
            33333333A222433333333333338F338F00003333333333333A22433333333333
            3338F38F000033333333333333A223333333333333338F830000333333333333
            333A333333333333333338330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
        end
        object BitCancelar: TBitBtn
          Left = 303
          Top = 305
          Width = 128
          Height = 25
          Caption = 'Cancelar'
          TabOrder = 7
          OnClick = BitCancelarClick
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333333333000033338833333333333333333F333333333333
            0000333911833333983333333388F333333F3333000033391118333911833333
            38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
            911118111118333338F3338F833338F3000033333911111111833333338F3338
            3333F8330000333333911111183333333338F333333F83330000333333311111
            8333333333338F3333383333000033333339111183333333333338F333833333
            00003333339111118333333333333833338F3333000033333911181118333333
            33338333338F333300003333911183911183333333383338F338F33300003333
            9118333911183333338F33838F338F33000033333913333391113333338FF833
            38F338F300003333333333333919333333388333338FFF830000333333333333
            3333333333333333333888330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
        end
        object mmoDesc: TMemo
          Left = 83
          Top = 165
          Width = 185
          Height = 108
          Lines.Strings = (
            '')
          TabOrder = 8
        end
        object EdtEmin: TEdit
          Left = 108
          Top = 125
          Width = 121
          Height = 21
          TabOrder = 9
        end
      end
    end
    object Editar: TTabSheet
      Caption = 'Editar/Excluir'
      ImageIndex = 1
      OnShow = EditarShow
      ExplicitWidth = 594
      ExplicitHeight = 544
      object Splitter2: TSplitter
        Left = 0
        Top = 345
        Width = 534
        Height = 3
        Cursor = crVSplit
        Align = alTop
        ExplicitTop = 289
        ExplicitWidth = 243
      end
      object GridAtualizar: TStringGrid
        Left = 0
        Top = 348
        Width = 534
        Height = 184
        Align = alClient
        FixedCols = 0
        TabOrder = 0
        OnSelectCell = GridAtualizarSelectCell
        ExplicitLeft = 24
        ExplicitTop = 400
        ExplicitWidth = 537
        ExplicitHeight = 105
      end
      object ScrollBox2: TScrollBox
        Left = 0
        Top = 0
        Width = 534
        Height = 345
        Align = alTop
        TabOrder = 1
        object Label9: TLabel
          Left = 18
          Top = 23
          Width = 87
          Height = 13
          Caption = 'Nome do produto:'
        end
        object Label10: TLabel
          Left = 18
          Top = 152
          Width = 50
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label12: TLabel
          Left = 18
          Top = 117
          Width = 99
          Height = 13
          Caption = 'Pre'#231'o de custo (R$):'
        end
        object Label13: TLabel
          Left = 265
          Top = 117
          Width = 103
          Height = 13
          Caption = 'Pre'#231'o de venda (R$):'
        end
        object Label14: TLabel
          Left = 18
          Top = 90
          Width = 122
          Height = 13
          Caption = 'Quantidade em estoque :'
        end
        object Label15: TLabel
          Left = 233
          Top = 57
          Width = 51
          Height = 13
          Caption = 'Categoria:'
        end
        object Label16: TLabel
          Left = 18
          Top = 57
          Width = 59
          Height = 13
          Caption = 'Fornecedor:'
        end
        object Label17: TLabel
          Left = 374
          Top = 21
          Width = 37
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label18: TLabel
          Left = 267
          Top = 152
          Width = 26
          Height = 13
          Caption = 'Foto:'
        end
        object Foto1: TImage
          Left = 312
          Top = 167
          Width = 173
          Height = 108
          Stretch = True
        end
        object Label19: TLabel
          Left = 267
          Top = 93
          Width = 78
          Height = 13
          Caption = 'Estoque m'#237'nimo:'
        end
        object EdtNome1: TEdit
          Left = 111
          Top = 18
          Width = 193
          Height = 21
          TabOrder = 0
        end
        object EdtPCusto1: TEdit
          Left = 123
          Top = 114
          Width = 100
          Height = 21
          TabOrder = 1
        end
        object EdtPVenda1: TEdit
          Left = 374
          Top = 114
          Width = 100
          Height = 21
          TabOrder = 2
        end
        object EdtQntd1: TEdit
          Left = 146
          Top = 87
          Width = 100
          Height = 21
          TabOrder = 3
        end
        object EdtCategoria1: TEdit
          Left = 290
          Top = 54
          Width = 142
          Height = 21
          TabOrder = 4
        end
        object cmbFornecedor1: TComboBox
          Left = 83
          Top = 54
          Width = 121
          Height = 21
          ItemHeight = 13
          TabOrder = 5
          OnChange = cmbFornecedorChange
        end
        object EdtCodigo: TEdit
          Left = 417
          Top = 18
          Width = 100
          Height = 21
          Color = clInactiveCaption
          Enabled = False
          TabOrder = 6
        end
        object BitAtualizar: TBitBtn
          Left = 34
          Top = 297
          Width = 119
          Height = 25
          Caption = 'Atualizar'
          Default = True
          TabOrder = 7
          OnClick = BtnAtualizarClick
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333333333333330000333333333333333333333333F33333333333
            00003333344333333333333333388F3333333333000033334224333333333333
            338338F3333333330000333422224333333333333833338F3333333300003342
            222224333333333383333338F3333333000034222A22224333333338F338F333
            8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
            33333338F83338F338F33333000033A33333A222433333338333338F338F3333
            0000333333333A222433333333333338F338F33300003333333333A222433333
            333333338F338F33000033333333333A222433333333333338F338F300003333
            33333333A222433333333333338F338F00003333333333333A22433333333333
            3338F38F000033333333333333A223333333333333338F830000333333333333
            333A333333333333333338330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
        end
        object BitBtn1: TBitBtn
          Left = 214
          Top = 297
          Width = 119
          Height = 25
          Caption = 'Cancelar'
          TabOrder = 8
          OnClick = BitCancelarClick
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333333333000033338833333333333333333F333333333333
            0000333911833333983333333388F333333F3333000033391118333911833333
            38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
            911118111118333338F3338F833338F3000033333911111111833333338F3338
            3333F8330000333333911111183333333338F333333F83330000333333311111
            8333333333338F3333383333000033333339111183333333333338F333833333
            00003333339111118333333333333833338F3333000033333911181118333333
            33338333338F333300003333911183911183333333383338F338F33300003333
            9118333911183333338F33838F338F33000033333913333391113333338FF833
            38F338F300003333333333333919333333388333338FFF830000333333333333
            3333333333333333333888330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
        end
        object mmoDesc1: TMemo
          Left = 74
          Top = 167
          Width = 167
          Height = 108
          Lines.Strings = (
            '')
          TabOrder = 9
        end
        object BitBtn2: TBitBtn
          Left = 385
          Top = 297
          Width = 119
          Height = 25
          Cancel = True
          Caption = 'Excluir'
          TabOrder = 10
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333FFFFF333333000033333388888833333333333F888888FFF333
            000033338811111188333333338833FFF388FF33000033381119999111833333
            38F338888F338FF30000339119933331111833338F388333383338F300003391
            13333381111833338F8F3333833F38F3000039118333381119118338F38F3338
            33F8F38F000039183333811193918338F8F333833F838F8F0000391833381119
            33918338F8F33833F8338F8F000039183381119333918338F8F3833F83338F8F
            000039183811193333918338F8F833F83333838F000039118111933339118338
            F3833F83333833830000339111193333391833338F33F8333FF838F300003391
            11833338111833338F338FFFF883F83300003339111888811183333338FF3888
            83FF83330000333399111111993333333388FFFFFF8833330000333333999999
            3333333333338888883333330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
        end
        object EdtEmin1: TEdit
          Left = 351
          Top = 90
          Width = 121
          Height = 21
          TabOrder = 11
        end
      end
    end
  end
end
