unit UUsuario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, ComCtrls, ToolWin, ImgList,SQLiteTable3,SqlTabGrid,
  UDbMod, Buttons, ExtCtrls, Mask, Types, StrUtils;
type
  TTUsuario = class(TForm)
    PageControl1: TPageControl;
    Inserir: TTabSheet;
    Editar: TTabSheet;
    GridInserir: TStringGrid;
    GridAtualizar: TStringGrid;
    ScrollBox1: TScrollBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Edit1: TEdit;
    ComboBox1: TComboBox;
    Edit3: TEdit;
    Edit2: TEdit;
    BtnInserir: TBitBtn;
    BtnCancelar: TBitBtn;
    Splitter1: TSplitter;
    ScrollBox2: TScrollBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Edit4: TEdit;
    ComboBox2: TComboBox;
    Edit5: TEdit;
    Edit6: TEdit;
    BtnAtualizar: TBitBtn;
    BtnExcluir: TBitBtn;
    Label9: TLabel;
    EdtCodigo: TEdit;
    Splitter2: TSplitter;



    procedure InserirShow(Sender: TObject);
    procedure BtnInserirClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure EditarShow(Sender: TObject);
    procedure GridAtualizarSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
    procedure BtnAtualizarClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TUsuario: TTUsuario;
  T:TSQLiteUniTable;
  SQL:string;

implementation

{$R *.dfm}
function Rot13(senha:string):string;
var
   i:integer;
   c:char;
   resultado:string;
begin
   for i := 1 to Length(senha) do begin
       c:=senha[i];
       if(c>='A') and (c<='M') then c:=chr(ord(c)+13)
       else if (c>='N') and (c<='Z') then c:=chr(ord(c)-13);
       resultado:=resultado+c
   end;
   Rot13:=resultado


end;

procedure TTUsuario.BtnAtualizarClick(Sender: TObject);
begin
    if  (Edit4.Text='') or (Edit5.Text='') or (ComboBox2.Text='') or (Edit6.Text='')
     then begin
     showmessage('� necess�rio preencher todos os campos!');
     end;

     if  (UpperCase(Edit5.Text)) <> (UpperCase(Edit6.Text)) then begin
     showmessage('Senhas n�o conferem!') ;
     end;

    SQL:='UPDATE USUARIO SET NOME = "'+Edit4.Text+'", TIPO = "'+ComboBox2.Text+'"';
    SQL:= SQL+',SENHA = "'+(UpperCase(Rot13(Edit5.Text)))+'" WHERE UN = "'+EdtCodigo.Text+'"';
    B.execSQL(SQL);

    showmessage('Usu�rio atualizado com sucesso!');

    BtnCancelarClick(Sender)   ;
    SQL:='SELECT UN AS CODIGO, NOME,TIPO,SENHA FROM USUARIO';
    T:=B.GetUniTable(SQL);
    ExecTableToGrid(T,GridAtualizar);
    GridAtualizar.FixedRows:=1;
    T.Free;


end;

procedure TTUsuario.BtnCancelarClick(Sender: TObject);
begin
    Edit1.Clear;
    Edit2.Clear;
    Edit3.Clear;
    ComboBox1.Clear;
end;

procedure TTUsuario.BtnInserirClick(Sender: TObject);

begin
 if  (Edit1.Text='') or (Edit2.Text='') or (ComboBox1.Text='') or (Edit3.Text='')
     then begin
     showmessage('� necess�rio preencher todos os campos!');
 end;


 
  if  (Edit2.Text) <> (Edit3.Text) then
      showmessage('Senhas n�o conferem!') ;
      exit;

    SQL:='INSERT INTO USUARIO VALUES (null,"'+Edit1.Text+'","'+ComboBox1.Text+'","'+Rot13(UpperCase(Edit3.Text))+'")';
    B.execSQL(SQL);

    showmessage('Usu�rio adicionado com sucesso!');

    BtnCancelarClick(Sender)   ;
    SQL:='SELECT NOME,TIPO,SENHA FROM USUARIO';
    T:=B.GetUniTable(SQL);
    ExecTableToGrid(T,GridInserir);
    GridInserir.FixedRows:=1;
    T.Free;

  end;

procedure TTUsuario.InserirShow(Sender: TObject);
begin
  SQL:='SELECT NOME,TIPO,SENHA FROM USUARIO';
  T:=B.GetUniTable(SQL);
  ExecTableToGrid(T,GridInserir);
  GridInserir.FixedRows:=1;
  T.Free
end;



procedure TTUsuario.EditarShow(Sender: TObject);
begin
  SQL:='SELECT UN AS CODIGO, NOME,TIPO,SENHA FROM USUARIO';
  T:=B.GetUniTable(SQL);
  ExecTableToGrid(T,GridAtualizar);
  GridAtualizar.FixedRows:=1;
  T.Free
end;


procedure TTUsuario.GridAtualizarSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);


begin
   if Arow =0 then  exit;
   EdtCodigo.Text:= GridAtualizar.Cells[0,Arow];
   Edit4.Text:=GridAtualizar.Cells[1,Arow];
   ComboBox2.Text:= GridAtualizar.Cells[2,Arow];
   Edit5.Text:= GridAtualizar.Cells[3,Arow] ;
   Edit6.Text:= GridAtualizar.Cells[3,Arow] ;

end;


end.
