unit UProdutos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, ComCtrls, ToolWin, ImgList,SQLiteTable3,SqlTabGrid,
  UDbMod, Buttons, ExtCtrls, Mask, Types, StrUtils, ExtDlgs;

type
  TTProdutos = class(TForm)
    PageControl1: TPageControl;
    Inserir: TTabSheet;
    Editar: TTabSheet;
    GridInserir: TStringGrid;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    EdtNome1: TEdit;
    EdtDescricao1: TEdit;
    EdtPCusto1: TEdit;
    EdtPVenda1: TEdit;
    EdtQntd1: TEdit;
    EdtCategoria1: TEdit;
    
    BitBtn1: TBitBtn;
    BitAtualizar: TBitBtn;
    GridAtualizar: TStringGrid;
    Label16: TLabel;
    ComboBox1: TComboBox;
    EdtCodigo: TEdit;
    Label17: TLabel;
    ScrollBox1: TScrollBox;
    Label1: TLabel;
    EdtNome: TEdit;
    Label4: TLabel;
    EdtPCusto: TEdit;
    Label5: TLabel;
    EdtPVenda: TEdit;
    Label2: TLabel;
    Label8: TLabel;
    cmbFornecedor: TComboBox;
    Splitter1: TSplitter;
    Label6: TLabel;
    EdtQuant: TEdit;
    Label7: TLabel;
    cmbCategoria: TComboBox;
    Label18: TLabel;
    BitConfirmar: TBitBtn;
    BitCancelar: TBitBtn;
    EdtEmin: TEdit;
    MmoDesc: TMemo;
    Foto: TImage;
    Label3: TLabel;
    OpenPictureDialog1: TOpenPictureDialog;
    procedure BitCancelarClick(Sender: TObject);
    procedure InserirShow(Sender: TObject);
    procedure EditarShow(Sender: TObject);
    procedure GridAtualizarSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
    procedure BitConfirmarClick(Sender: TObject);


  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TProdutos: TTProdutos;
  T:TSQLiteUniTable;
  SQL:string;

implementation

{$R *.dfm}

procedure TTProdutos.BitConfirmarClick(Sender: TObject);
var
  fn:integer;
begin
   SQL:='SELECT FN FROM FORNECEDOR WHERE FNOME="'+cmbFornecedor.Text+'"';
   T:=B.GetUniTable(SQL);
   fn:=T.FieldAsInteger(0);

   SQL:='INSERT INTO PRODUTO VALUES(NULL,"'+UpperCase(EdtNome.Text)+'","'+MmoDesc.Text+'",';
   SQL:=SQL+'NULL,"'+EdtPCusto.Text+'","'+EdtVCusto.Text+'","'+EdtQuant.Text+'","'+EdtEmin.Text+'", ';
   SQL:=SQL+'"'+cmbCategoria.Text+'","'+fn+'")';
   Try
     B.ExecSQL(SQL);
     ShowMessage('Produto cadastrado com sucesso!');
   Except
     on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
   End;

end;

procedure TTProdutos.BitCancelarClick(Sender: TObject);
begin
    EdtNome.Clear;
    cmbFornecedor.Text:='';
    cmbCategoria.Text:='';
    EdtEmin.Clear;
    EdtPCusto.Clear;
    EdtPVenda.Clear;
    EdtQuant.Clear;
    MmoDesc.Clear;
    Foto.Visible:=false



end;

procedure TTProdutos.InserirShow(Sender: TObject);
begin

  SQL:='SELECT DISTINCT FNOME FROM FORNECEDOR';
  T:=B.GetUniTable(SQL);

  While not T.EOF do begin
    cmbFornecedor.Items.Add(T.FieldAsString(0));
    T.Next
  end;

  SQL:='SELECT PRNOME AS NOME,DESCRICAO,PRECOC AS PRECO_CUSTO, PRECOV PRECO_VENDA,ESTOQUE ,ESTOQUEMIN AS ESTOQUE_MINIMO ,CATEGORIA FROM PRODUTO';
  T:=B.GetUniTable(SQL);
  ExecTableToGrid(T,GridInserir);
  GridInserir.FixedRows:=1;
  T.Free
end;

procedure TTProdutos.EditarShow(Sender: TObject);
begin
  SQL:='SELECT PRN AS CODIGO,PRNOME AS NOME, DESCRICAO,PRECOC AS CUSTO, PRECOV AS VENDA,ESTOQUE AS ESTOQUE_INICIAL,ESTOQUEMIN AS QNTD_ATUAL,CATEGORIA FROM PRODUTO;'  ;
  T:=B.GetUniTable(SQL);
  ExecTableToGrid(T,GridAtualizar);
  GridAtualizar.FixedRows:=1;
  T.Free
end;
procedure TTProdutos.GridAtualizarSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
   if Arow =0 then  exit;
   EdtCodigo.Text:=GridAtualizar.Cells[0,Arow];
   EdtNome1.Text:= GridAtualizar.Cells[1,Arow];
   EdtDescricao1.Text:= GridAtualizar.Cells[2,Arow] ;
   EdtPCusto1.Text:= GridAtualizar.Cells[3,Arow] ;
   EdtPVenda1.Text:=  GridAtualizar.Cells[4,Arow] ;
   EdtQntd1.Text:=  GridAtualizar.Cells[6,Arow] ;
   EdtCategoria1.Text:= GridAtualizar.Cells[7,Arow];

end;
end.


