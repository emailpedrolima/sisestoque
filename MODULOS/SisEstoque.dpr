program SisEstoque;

uses
  Forms,
  UPrincipal in 'UPrincipal.pas' {TPrincipal},
  UFornecedores in 'UFornecedores.pas' {TFornecedores},
  UPedidos in 'UPedidos.pas' {TPedidos},
  USobre in 'USobre.pas' {TSobre},
  UDbmod in 'UDbmod.pas',
  USenha in 'USenha.pas' {TSenha},
  UComoUsar in 'UComoUsar.pas' {TComoUsar},
  URelatorios in 'URelatorios.pas' {TRelatorios},
  SQLite3 in '..\..\APP\SQLite3.pas',
  sqlitetable3 in '..\..\APP\sqlitetable3.pas',
  SqlTabGrid in '..\..\APP\SqlTabGrid.pas',
  UClientes in 'UClientes.pas' {TClientes},
  UTrocaSenha in 'UTrocaSenha.pas' {TTrocaSenha},
  UProdutos in 'UProdutos.pas' {TProdutos},
  UUsuario in 'UUsuario.pas' {TUsuario};

begin
  Application.Initialize;
  Application.CreateForm(TTSenha, TSenha);
  Application.CreateForm(TTPrincipal, TPrincipal);
  Application.CreateForm(TTFornecedores, TFornecedores);
  Application.CreateForm(TTPedidos, TPedidos);
  Application.CreateForm(TTSobre, TSobre);
  Application.CreateForm(TTComoUsar, TComoUsar);
  Application.CreateForm(TTRelatorios, TRelatorios);
  Application.CreateForm(TTClientes, TClientes);
  Application.CreateForm(TTTrocaSenha, TTrocaSenha);
  Application.CreateForm(TTProdutos, TProdutos);
  Application.CreateForm(TTUsuario, TUsuario);
  Application.Run;
end.


