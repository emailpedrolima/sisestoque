unit UPedidos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls,UDbMod,SQLiteTable3,SqlTabGrid, CheckLst,
  Grids, ValEdit, Buttons,StrUtils;

type
  TTPedidos= class(TForm)
    PageControl1: TPageControl;
    Inserir: TTabSheet;
    Editar: TTabSheet;
    ScrollBox1: TScrollBox;
    Label15: TLabel;
    EdtPedido1: TEdit;
    Label16: TLabel;
    Data1: TDateTimePicker;
    Label17: TLabel;
    cmbPG1: TComboBox;
    Label18: TLabel;
    cmbStatus1: TComboBox;
    Label19: TLabel;
    cmbCliente1: TComboBox;
    Label20: TLabel;
    EdtDesconto1: TEdit;
    Label21: TLabel;
    EdtValor1: TEdit;
    GroupBox2: TGroupBox;
    clItens1: TCheckListBox;
    BtnAdd1: TButton;
    Button4: TButton;
    GridPedido1: TStringGrid;
    BtnAtualizar: TBitBtn;
    BitBtn2: TBitBtn;
    BtnExcluir: TBitBtn;
    Splitter1: TSplitter;
    GridAtualizar: TStringGrid;
    ScrollBox2: TScrollBox;
    Panel1: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    cmbPG: TComboBox;
    cmbStatus: TComboBox;
    cmbCliente: TComboBox;
    EdtDesconto: TEdit;
    EdtValor: TEdit;
    GroupBox1: TGroupBox;
    clItens: TCheckListBox;
    GridPedido: TStringGrid;
    Data: TDateTimePicker;
    BtnGeraPedido: TBitBtn;
    BtnCancelar: TBitBtn;
    Splitter2: TSplitter;
    GridInserir: TStringGrid;
    BitBtn1: TBitBtn;
    BitBtn3: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure BtnGeraPedidoClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure InserirShow(Sender: TObject);
    procedure EditarShow(Sender: TObject);
    procedure GridAtualizarSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure BtnExcluirClick(Sender: TObject);
    procedure BtnAtualizarClick(Sender: TObject);
    procedure BtnAdd1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TPedidos: TTPedidos;

implementation
var
   T,Taux:TSQLiteUniTable;
   SQL:string;

{$R *.dfm}

procedure DeleteRow(ARowIndex: Integer; AGrid: TStringGrid);
var
  i, j: Integer;
begin
  with AGrid do
  begin
    if (ARowIndex = RowCount) then
      RowCount := RowCount - 1
    else
    begin
      for i := ARowIndex to RowCount do
        for j := 0 to ColCount do
          Cells[j, i] := Cells[j, i + 1];

      RowCount := RowCount - 1;
    end;
  end;
end;

procedure TTPedidos.BitBtn1Click(Sender: TObject);
var
   i,x:integer;
begin
   for i := 0 to clItens.Items.Count -1 do begin
      if clItens.Checked[i] then begin
        GridPedido.RowCount := GridPedido.RowCount + 1;
        x:=GridPedido.RowCount-2;
        GridPedido.Cells[0,x]:=clItens.Items[i];

        SQL:='SELECT PRECOV FROM PRODUTO WHERE PRNOME="'+clItens.Items[i]+'"';
        T:=B.GetUniTable(SQL);
        GridPedido.Cells[1,x]:=T.FieldAsString(0)
      end;
   end;

   for i:= clItens.Items.Count -1 downto 0 do
       if clItens.Checked[i] then clItens.Items.Delete(i);

end;

procedure TTPedidos.BitBtn3Click(Sender: TObject);
begin
  clItens.Items.Add(GridPedido.Cells[GridPedido.Col,GridPedido.Row]);
  clItens.Sorted:=true;
  DeleteRow(GridPedido.Row,GridPedido)
end;

procedure TTPedidos.BtnAdd1Click(Sender: TObject);
var
   i,x:integer;
begin
for i := 0 to clItens1.Items.Count -1 do begin
      if clItens1.Checked[i] then begin
        GridPedido1.RowCount := GridPedido1.RowCount + 1;
        x:=GridPedido1.RowCount-2;
        GridPedido1.Cells[0,x]:=clItens1.Items[i];

        SQL:='SELECT PRECOV FROM PRODUTO WHERE PRNOME="'+clItens1.Items[i]+'"';
        T:=B.GetUniTable(SQL);
        GridPedido1.Cells[1,x]:=T.FieldAsString(0)
      end;
   end;

   for i:= clItens1.Items.Count -1 downto 0 do
       if clItens1.Checked[i] then clItens1.Items.Delete(i);
end;

procedure TTPedidos.BtnAtualizarClick(Sender: TObject);
var
   datap:string;
   i:integer;
begin  

   for i := 1 to GridPedido1.RowCount - 2 do  begin
       if GridPedido1.Cells[2,i]='0' then begin
          SQL:='DELETE FROM CONTEM WHERE PN="'+EdtPedido1.Text+'" AND ';
          SQL:=SQL+' PRN=(SELECT PRN FROM PRODUTO WHERE PRNOME="'+GridPedido1.Cells[0,i]+'")';
          Try
            B.execSQL(SQL);
          Except
            on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
          End;
       end
       else  begin
          SQL:='SELECT QUANTIDADE FROM CONTEM WHERE PN="'+EdtPedido1.Text+'" AND ';
          SQL:=SQL+' PRN=(SELECT PRN FROM PRODUTO WHERE PRNOME="'+GridPedido1.Cells[0,i]+'")';
          T:=B.GetUniTable(SQL);
          if T.FieldIsNull(0)  then begin
            SQL:='SELECT PRN FROM PRODUTO WHERE PRNOME="'+GridPedido1.Cells[0,i]+'"';
            Taux:=B.GetUniTable(SQL);

            SQL:='INSERT INTO CONTEM VALUES("'+EdtPedido1.Text+'","'+Taux.FieldAsString(0)+'","'+GridPedido1.Cells[2,i]+'")';
            B.execSQL(SQL)
          end
          else begin
             SQL:='UPDATE CONTEM SET QUANTIDADE="'+GridPedido1.Cells[2,i]+'" ';
             SQL:=SQL+'WHERE PN="'+EdtPedido1.Text+'" AND ';
             SQL:=SQL+' PRN=(SELECT PRN FROM PRODUTO WHERE PRNOME="'+GridPedido1.Cells[0,i]+'")';
             Try
                B.execSQL(SQL);
             Except
                on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
             End;
          end;
       end;
    end;

   datap:=formatdatetime('yyyy-MM-dd',Data1.DateTime);
   SQL:='UPDATE PEDIDO SET DATA="'+datap+'", FORMAPG="'+cmbPG1.Text+'",STATUS="'+cmbStatus1.Text+'",';
   SQL:=SQL+' DESCONTO="'+EdtDesconto1.Text+'" WHERE PN="'+EdtPedido1.Text+'"';
   Try
     B.execSQL(SQL);
     ShowMessage('Pedido atualizado com sucesso!');
   Except
     on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
   End;


    SQL:='SELECT P.*,C.CNOME FROM PEDIDO P JOIN CLIENTE C USING(CN)';
    T:=B.GetUniTable(SQL);
    ExecTableToGrid(T,GridAtualizar);
    GridAtualizar.FixedRows:=1


end;

procedure TTPedidos.BtnCancelarClick(Sender: TObject);
var
   i:integer;
begin

   Data.Date:=Now;
   cmbPG.Text:='';
   cmbStatus.Text:='';
   cmbCliente.Text:='';
   EdtDesconto.Clear;
   EdtValor.Clear;

   for i := 1 to GridPedido.RowCount - 2 do
      GridPedido.Rows[i].Clear;
   GridPedido.RowCount:=2;
   GridPedido.Cells[0,0]:='Item';
   GridPedido.Cells[1,0]:='Pre�o Unit�rio (R$)';
   GridPedido.Cells[2,0]:='Quantidade';

   EdtPedido1.Clear;
   Data1.Date:=Now;
   cmbPG1.Text:='';
   cmbStatus1.Text:='';
   cmbCliente1.Text:='';
   EdtDesconto1.Clear;
   EdtValor1.Clear;
   clItens1.Clear;

    for i := 0 to GridPedido1.RowCount - 1 do
      GridPedido1.Rows[i].Clear;
   GridPedido1.RowCount:=2;
   GridPedido1.Cells[0,0]:='Item';
   GridPedido1.Cells[1,0]:='Pre�o Unit�rio (R$)';
   GridPedido1.Cells[2,0]:='Quantidade';



end;

procedure TTPedidos.BtnExcluirClick(Sender: TObject);
var
  confirma:integer;
begin
  confirma:=MessageDlg('Deseja excluir o registro selecionado?',mtWarning,mbYesNo,0);
  if confirma=6 then begin
    SQL:='DELETE FROM CONTEM WHERE PN="'+EdtPedido1.Text+'" ';
    Try
       B.execSQL(SQL);
    Except
       on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
    End;

    SQL:='DELETE FROM PEDIDO WHERE PN="'+EdtPedido1.Text+'"';
    Try
       B.execSQL(SQL);
       ShowMessage('Pedido exclu�do com sucesso!');
    Except
       on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
    End;


    SQL:='SELECT P.*,C.CNOME FROM PEDIDO P JOIN CLIENTE C USING(CN)';
    T:=B.GetUniTable(SQL);
    ExecTableToGrid(T,GridAtualizar);
    GridAtualizar.FixedRows:=1
  end;

end;

procedure TTPedidos.BtnGeraPedidoClick(Sender: TObject);
var
   i,cliente:integer;
   datap,codp,codprod:string;
begin

   for i := 1 to  GridPedido.RowCount-2 do   begin
      if GridPedido.Cells[2,i] = '' then begin
        ShowMessage('Cada item selecionado deve possuir quantidade superior a 0!');
        exit
      end;
   end;

   SQL:='SELECT CN FROM CLIENTE WHERE CNOME="'+cmbCliente.Text+'"';
   T:=B.GetUniTable(SQL);
   cliente:=T.FieldAsInteger(0);

   datap:=formatdatetime('yyyy-MM-dd',Data.DateTime);

   SQL:='INSERT INTO PEDIDO VALUES(NULL,NULL,"'+cmbPG.Text+'","'+datap+'",';
   SQL:=SQL+ '"EM PROCESSAMENTO","'+EdtDesconto.Text+'","'+IntToStr(cliente)+'")';
   Try
       B.execSQL(SQL);
    Except
       on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
    End;

   SQL:='SELECT last_insert_rowid()';
   T:=B.GetUniTable(SQL);     
   codp:=IntToStr(T.FieldAsInteger(0));


   for i := 1 to  GridPedido.RowCount-2 do   begin
      SQL:='SELECT PRN FROM PRODUTO WHERE PRNOME="'+GridPedido.Cells[0,i]+'"';
      T:=B.GetUniTable(SQL);
      codprod:=IntToStr(T.FieldAsInteger(0)); 
      SQL:='INSERT INTO CONTEM VALUES("'+codp+'","'+codprod+'","'+GridPedido.Cells[2,i]+'")';
      Try
          B.execSQL(SQL);
      Except
          on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
      End;

   end;

   SQL:='SELECT VALOR FROM PEDIDO WHERE PN="'+codp+'"';
   T:=B.GetUniTable(SQL);
   EdtValor.Text:=FloatToStr(T.FieldAsInteger(0));


   SQL:='UPDATE PEDIDO SET STATUS="'+cmbStatus.Text+'" WHERE PN="'+codp+'"';
   Try
       B.execSQL(SQL);
       ShowMessage('Pedido gerado com sucesso!');
    Except
       on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
    End;



  SQL:='SELECT P.*,C.CNOME,PR.PRNOME,CO.QUANTIDADE FROM PEDIDO P JOIN CLIENTE C USING(CN)';
  SQL:=SQL+'JOIN CONTEM CO USING(PN) JOIN PRODUTO PR USING(PRN)';
  T:=B.GetUniTable(SQL);
  ExecTableToGrid(T,GridInserir);
  GridInserir.FixedRows:=1;
  T.Free;

  BtnCancelarClick(Sender)

end;

procedure TTPedidos.EditarShow(Sender: TObject);
begin
  SQL:='SELECT P.*,C.CNOME FROM PEDIDO P JOIN CLIENTE C USING(CN)';
  T:=B.GetUniTable(SQL);
  ExecTableToGrid(T,GridAtualizar);
  GridAtualizar.FixedRows:=1;
  T.Free;

  SQL:='SELECT DISTINCT CNOME FROM CLIENTE';
  T:=B.GetUniTable(SQL);

  While not T.EOF do begin
    cmbCliente1.Items.Add(T.FieldAsString(0));
    T.Next
  end;

  GridPedido1.Cells[0,0]:='Item';
  GridPedido1.Cells[1,0]:='Pre�o Unit�rio (R$)';
  GridPedido1.Cells[2,0]:='Quantidade';
  Data1.Date:=Now
end;

procedure TTPedidos.FormShow(Sender: TObject);
begin
   PageControl1.ActivePageIndex:=0;
end;

procedure TTPedidos.GridAtualizarSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
d,x:Ansistring;
i:integer;
begin
   if Arow =0 then  exit;

   clItens1.Clear;
   for i := 0 to GridPedido1.RowCount - 1 do
      GridPedido1.Rows[i].Clear;
   GridPedido1.RowCount:=2;
   GridPedido1.Cells[0,0]:='Item';
   GridPedido1.Cells[1,0]:='Pre�o Unit�rio (R$)';
   GridPedido1.Cells[2,0]:='Quantidade';

   d:=GridAtualizar.Cells[3,Arow];
   x := AnsiRightStr(d,2)+'/'+AnsiMidStr(d,6, 2)+'/'+AnsiLeftStr(d,4);

   EdtPedido1.Text:=GridAtualizar.Cells[0,Arow];
   Data1.Date:= StrToDate(x);
   cmbPG1.Text:= GridAtualizar.Cells[2,Arow] ;
   cmbStatus1.Text:= GridAtualizar.Cells[4,Arow] ;
   cmbCliente1.Text:=  GridAtualizar.Cells[7,Arow] ;
   EdtDesconto1.Text:=  GridAtualizar.Cells[5,Arow] ;
   EdtValor1.Text:= GridAtualizar.Cells[1,Arow];      

   SQL:='SELECT DISTINCT PRNOME FROM PRODUTO ';
   SQL:=SQL+'WHERE PRN NOT IN (SELECT PRN FROM CONTEM WHERE PN="'+ GridAtualizar.Cells[0,Arow]+'")';
   T:=B.GetUniTable(SQL);

   While not T.EOF do begin
    clItens1.Items.Add(T.FieldAsString(0));
    T.Next
   end;


   SQL:='SELECT PR.PRNOME,PR.PRECOV,C.QUANTIDADE FROM PRODUTO PR JOIN CONTEM C USING(PRN) ';
   SQL:=SQL+'WHERE C.PN="'+EdtPedido1.Text+'"';
   T:=B.GetUniTable(SQL);

   i:=1;
   GridPedido1.RowCount :=2;
   While not T.EOF do begin

    GridPedido1.RowCount := GridPedido1.RowCount + 1;
    GridPedido1.Cells[0,i]:=T.FieldAsString(0);
    GridPedido1.Cells[1,i]:=T.FieldAsString(1);
    GridPedido1.Cells[2,i]:=T.FieldAsString(2);
    inc(i);
    T.Next
   end;


end;

procedure TTPedidos.InserirShow(Sender: TObject);
begin
  SQL:='SELECT P.*,C.CNOME FROM PEDIDO P JOIN CLIENTE C USING(CN)';
  //SQL:='SELECT P.*,C.CNOME,PR.PRNOME,CO.QUANTIDADE FROM PEDIDO P JOIN CLIENTE C USING(CN)';
  //SQL:=SQL+'JOIN CONTEM CO USING(PN) JOIN PRODUTO PR USING(PRN)';
  T:=B.GetUniTable(SQL);
  ExecTableToGrid(T,GridInserir);
  GridInserir.FixedRows:=1;
  T.Free;

  SQL:='SELECT DISTINCT CNOME FROM CLIENTE';
   T:=B.GetUniTable(SQL);

   While not T.EOF do begin
     cmbCliente.Items.Add(T.FieldAsString(0));
     T.Next
   end;

   SQL:='SELECT DISTINCT PRNOME FROM PRODUTO';
   T:=B.GetUniTable(SQL);

   While not T.EOF do begin
     clItens.Items.Add(T.FieldAsString(0));
     T.Next
   end;

   GridPedido.Cells[0,0]:='Item';
   GridPedido.Cells[1,0]:='Pre�o Unit�rio (R$)';
   GridPedido.Cells[2,0]:='Quantidade';   
   Data.Date:=Now

end;

end.
