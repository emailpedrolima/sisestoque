unit UPrincipal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus,jpeg, StdCtrls, ExtCtrls, ComCtrls;

type
    TTPrincipal = class(TForm)
    Image1: TImage;
    MenuAdm: TMainMenu;
    Cad1: TMenuItem;
    Fornecedores: TMenuItem;
    Produtos: TMenuItem;
    Usuarios: TMenuItem;
    MenuItem1: TMenuItem;
    Sobre2: TMenuItem;
    MenuUsuario: TMainMenu;
    Relatrios2: TMenuItem;
    Ajuda: TMenuItem;
    Fim: TMenuItem;
    Fim2: TMenuItem;
    Clientes2: TMenuItem;
    Pedidos: TMenuItem;
    Estoque: TMenuItem;
    TotalGeral: TMenuItem;
    VendasProduto: TMenuItem;
    LucroVenda: TMenuItem;
    PedidosCliente: TMenuItem;
    PedidosAbertos: TMenuItem;
    MaisVendidos: TMenuItem;
    Estoquedeprodutos1: TMenuItem;
    Listadepedidosemaberto1: TMenuItem;
    Listadepedidosporcliente1: TMenuItem;
    Listadeprodutosmaisvendidos1: TMenuItem;
    Lucroobtidoemcadavenda1: TMenuItem;
    otaldevendasporproduto1: TMenuItem;
    otalgeraldevendas1: TMenuItem;
    Listadepedidosporms1: TMenuItem;
    Listadepedidosporms2: TMenuItem;
    otaldevendasporms1: TMenuItem;
    StatusBar1: TStatusBar;
    Timer1: TTimer;


    procedure FornecedoresClick(Sender:TObject);
    procedure ProdutosClick(Sender:TObject);
    procedure PedidosClick(Sender:TObject);
    procedure SairClick(Sender:TObject);
    procedure FormShow(Sender: TObject);
    procedure FimClick(Sender: TObject);
    procedure EstoqueClick(Sender: TObject);
    procedure TotalGeralClick(Sender: TObject);
    procedure VendasProdutoClick(Sender: TObject);
    procedure LucroVendaClick(Sender: TObject);
    procedure PedidosClienteClick(Sender: TObject);
    procedure PedidosAbertosClick(Sender: TObject);
    procedure MaisVendidosClick(Sender: TObject);
    procedure Listadepedidosporms1Click(Sender: TObject);
    procedure otaldevendasporms1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Clientes2Click(Sender: TObject);
    procedure UsuariosClick(Sender: TObject);
    procedure Sobre2Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TPrincipal : TTPrincipal;
  titulo,subtitulo:string;
  SQL:ansistring;

implementation
uses UFornecedores, UProdutos, UClientes, UPedidos, USobre, USenha, UComoUsar,
  URelatorios, UUsuario;

{$R *.dfm}


procedure TTPrincipal.FimClick(Sender: TObject);
begin
   Application.Terminate
end;

procedure TTPrincipal.FormShow(Sender: TObject);
begin
  if tpusuario='ADM' then TPrincipal.Menu:=MenuAdm
  else  TPrincipal.Menu:=MenuUsuario;

  StatusBar1.Panels[3].Text:=usuario
  


end;

procedure TTPrincipal.FornecedoresClick(Sender: TObject);
begin
     TFornecedores.ShowModal;
end;

procedure TTPrincipal.Listadepedidosporms1Click(Sender: TObject);
begin

  titulo:='Lista de vendas por m�s';
  subtitulo:='Lista de vendas realizadas no m�s selecionado';
  TRelatorios.ShowModal
end;

procedure TTPrincipal.LucroVendaClick(Sender: TObject);
begin
  SQL:='SELECT P.PN AS PEDIDO, CAST(SUM(L.LUCRO) AS DECIMAL(4,2)) AS LUCRO FROM PEDIDO P JOIN (SELECT P.PN, C.QUANTIDADE*(PR.PRECOV-PR.PRECOC) AS LUCRO ';
  SQL:=SQL+ 'FROM PRODUTO PR JOIN CONTEM C USING (PRN) JOIN PEDIDO P USING (PN) WHERE STATUS="APROVADO" ) AS L USING (PN) GROUP BY P.PN ';
  titulo:='Lucro obtido em cada venda';
  subtitulo:='Lucro obtido, em R$, em cada venda';
  TRelatorios.ShowModal
end;

procedure TTPrincipal.MaisVendidosClick(Sender: TObject);
begin
  SQL:='SELECT  PRODUTO.PRN,PRODUTO.PRNOME, SUM(QUANTIDADE) AS TOTAL  FROM CONTEM  JOIN PEDIDO USING (PN)  ';
  SQL:=SQL+ ' JOIN PRODUTO USING (PRN) WHERE PEDIDO.STATUS="APROVADO" GROUP BY PRN ORDER BY TOTAL DESC ';
  titulo:='Lista de produtos mais vendidos';
  subtitulo:='Lista de produtos mais vendidos ';
  TRelatorios.ShowModal
end;

procedure TTPrincipal.otaldevendasporms1Click(Sender: TObject);
begin
  titulo:='Total de vendas por m�s';
  subtitulo:='Total, em R$, de vendas realizadas no m�s selecionado';
  TRelatorios.ShowModal
end;

procedure TTPrincipal.ProdutosClick(Sender: TObject);
begin
  TProdutos.ShowModal;
end;

procedure TTPrincipal.Clientes2Click(Sender: TObject);
begin
  TClientes.ShowModal;
end;

procedure TTPrincipal.PedidosAbertosClick(Sender: TObject);
begin
  SQL:='SELECT P.PN AS NUMERO_PEDIDO,P.DATA,P.VALOR,C.CNOME, PR.PRNOME, CO.QUANTIDADE  FROM PEDIDO P JOIN CLIENTE C USING (CN) ';
  SQL:=SQL+ ' JOIN CONTEM CO USING (PN) JOIN PRODUTO PR USING (PRN) WHERE P.STATUS="EM PROCESSAMENTO" ';
  titulo:='Lista de pedidos em aberto';
  subtitulo:='Lista de pedidos com status igual a "EM PROCESSAMENTO" ';
  TRelatorios.ShowModal
end;

procedure TTPrincipal.PedidosClick(Sender: TObject);
begin
  TPedidos.ShowModal;
end;
             

procedure TTPrincipal.PedidosClienteClick(Sender: TObject);
begin
  SQL:='SELECT P.*,C.CNOME FROM PEDIDO P JOIN CLIENTE C USING (CN) ';
  titulo:='Lista de pedidos por cliente';
  subtitulo:='Lista de pedidos realizados por cada cliente';
  TRelatorios.ShowModal
end;

procedure TTPrincipal.Timer1Timer(Sender: TObject);
begin
    StatusBar1.Panels[4].Text:=DateTimeToStr(Now)
end;

procedure TTPrincipal.TotalGeralClick(Sender: TObject);
begin
  SQL:='SELECT SUM(VALOR) AS TOTAL_VENDAS FROM PEDIDO WHERE STATUS="APROVADO"';
  titulo:='Total geral de vendas';
  subtitulo:='Total geral, em R$, de vendas';
  TRelatorios.ShowModal
end;

procedure TTPrincipal.UsuariosClick(Sender: TObject);
begin
   TUsuario.ShowModal
end;

procedure TTPrincipal.VendasProdutoClick(Sender: TObject);
begin
  SQL:='SELECT  PRODUTO.PRNOME AS PRODUTO, PRODUTO.PRECOV *SUM(QUANTIDADE) AS TOTAL  FROM CONTEM  JOIN PEDIDO USING (PN) ';
  SQL:=SQL+'JOIN PRODUTO USING (PRN) WHERE PEDIDO.STATUS="APROVADO" GROUP BY PRN';
  titulo:='Total de vendas por produto';
  subtitulo:='Total, em R$, de vendas por produto';
  TRelatorios.ShowModal
end;

procedure TTPrincipal.EstoqueClick(Sender: TObject);
begin
  SQL:='SELECT * FROM PRODUTO';
  titulo:='Estoque de produtos';
  subtitulo:='Lista de produtos em estoque';
  TRelatorios.ShowModal
end;

procedure TTPrincipal.SairClick(Sender: TObject);
begin
  Application.Terminate
end;

procedure TTPrincipal.Sobre2Click(Sender: TObject);
begin
   TSobre.ShowModal
end;

end.
