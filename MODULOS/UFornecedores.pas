unit UFornecedores;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, ComCtrls, ToolWin, ImgList,SQLiteTable3,SqlTabGrid,
  UDbMod, Buttons, ExtCtrls, Mask, Types, StrUtils;

type
  TTFornecedores = class(TForm)
    PageControl1: TPageControl;
    Inserir: TTabSheet;
    Panel1: TPanel;
    GridInserir: TStringGrid;
    Editar: TTabSheet;
    Panel2: TPanel;
    GridAtualizar: TStringGrid;
    Splitter1: TSplitter;
    ScrollBox1: TScrollBox;
    Panel3: TPanel;
    No: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EdtNome: TEdit;
    GroupBox1: TGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    EdtLog: TEdit;
    EdtNumero: TEdit;
    EdtBairro: TEdit;
    EdtCEP: TMaskEdit;
    EdtCidade: TEdit;
    CmbEstado: TComboBox;
    EdtTelefone: TMaskEdit;
    EdtEmail: TEdit;
    BtnInserir: TBitBtn;
    BtnCancelar: TBitBtn;
    Splitter2: TSplitter;
    ScrollBox2: TScrollBox;
    Label4: TLabel;
    EdtNome1: TEdit;
    Label8: TLabel;
    EdtCodigo: TEdit;
    Label6: TLabel;
    EdtTelefone1: TMaskEdit;
    Label7: TLabel;
    EdtEmail1: TEdit;
    BtnAtualizar: TBitBtn;
    BtnCancelar1: TBitBtn;
    Label5: TLabel;
    EdtComplemento: TEdit;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    EdtLog1: TEdit;
    EdtNumero1: TEdit;
    EdtBairro1: TEdit;
    EdtCEP1: TMaskEdit;
    EdtCidade1: TEdit;
    CmbEstado1: TComboBox;
    EdtComplemento1: TEdit;
    BtnExcluir: TBitBtn;
    GroupBox3: TGroupBox;
    Label21: TLabel;
    EdtCPF: TMaskEdit;
    Label22: TLabel;
    EdtCNPJ: TMaskEdit;
    GroupBox4: TGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    EdtCPF1: TMaskEdit;
    EdtCNPJ1: TMaskEdit;
    procedure BtnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure InserirShow(Sender: TObject);
    procedure EditarShow(Sender: TObject);
    procedure GridAtualizarSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure BtnInserirClick(Sender: TObject);
    procedure BtnAtualizarClick(Sender: TObject);
    procedure BtnExcluirClick(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TFornecedores: TTFornecedores;

implementation
var
   T:TSQLiteUniTable;
   SQL:string;

{$R *.dfm}

procedure TTFornecedores.BtnCancelarClick(Sender: TObject);
begin
     EdtNome.Clear;
     EdtLog.Clear;
     EdtNumero.Clear;
     EdtBairro.Clear;
     EdtCidade.Clear;
     EdtCEP.Clear;
     EdtTelefone.Clear;
     EdtEmail.Clear;
     CmbEstado.Text:='';
     EdtComplemento.Clear;
     EdtCPF.Clear;
     EdtCNPJ.Clear;
     EdtNome1.Clear;
     EdtLog1.Clear;
     EdtNumero1.Clear;
     EdtBairro1.Clear;
     EdtCidade1.Clear;
     EdtCEP1.Clear;
     EdtTelefone1.Clear;
     EdtEmail1.Clear;
     CmbEstado1.Text:='';
     EdtCodigo.Clear;
     EdtComplemento1.Clear;
     EdtCPF1.Clear;
     EdtCNPJ1.Clear;

end;

procedure TTFornecedores.BtnExcluirClick(Sender: TObject);
var
  confirma:integer;
begin
  confirma:=MessageDlg('Deseja excluir o registro selecionado?',mtWarning,mbYesNo,0);
  if confirma=6 then begin

    SQL:='DELETE FROM FORNECEDOR WHERE FN="'+EdtCodigo.Text+'"';
    Try
       B.execSQL(SQL);
       ShowMessage('Fornecedor exclu�do com sucesso!');
       SQL:='DELETE FROM ENDERECO WHERE ';
       SQL:=SQL+'EN=(SELECT END FROM FORNECEDOR WHERE FN="'+EdtCodigo.Text+'")';
       Try
          B.execSQL(SQL);
       Except
          on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
       End;
    Except
       on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
    End;




    SQL:='SELECT F.*,E.RUA,E.NUMERO,E.COMPLEMENTO,E.BAIRRO,E.CIDADE,E.ESTADO,E.CEP  FROM FORNECEDOR F JOIN ENDERECO E WHERE F.END=E.EN'  ;
    T:=B.GetUniTable(SQL);
    ExecTableToGrid(T,GridAtualizar);
    GridAtualizar.FixedRows:=1
  end;

end;

procedure TTFornecedores.BtnAtualizarClick(Sender: TObject);
var
  cep,tel,cpf,cnpj:string;
begin

  if EdtCEP1.EditText='_____-___' then  cep:=''
  else cep:=EdtCEP1.Text;
  if EdtTelefone1.EditText='(__)____-____' then tel:=''
  else tel:= EdtTelefone1.EditText;
  if EdtCPF1.EditText='___.___.___-__' then  cpf:=''
  else cpf:=EdtCPF1.Text;
  if EdtCNPJ1.EditText='__.___.___/____-__' then  cnpj:=''
  else cnpj:=EdtCNPJ1.Text;

  SQL:='UPDATE ENDERECO SET RUA="'+EdtLog1.Text+'", NUMERO="'+EdtNumero1.Text+'",COMPLEMENTO="'+EdtComplemento1.Text+'",';
  SQL:=SQL+' CIDADE="'+EdtCidade1.Text+'",ESTADO="'+CmbEstado1.Text+'",CEP="'+cep+'"';
  SQL:=SQL+ ' WHERE EN=(SELECT END FROM FORNECEDOR WHERE FN="'+EdtCodigo.Text+'")';
  Try
     B.execSQL(SQL);
  Except
     on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
  End;

  SQL:='UPDATE FORNECEDOR SET FNOME="'+EdtNome1.Text+'", TEL="'+tel+'",';
  SQL:=SQL+' EMAIL="'+EdtEmail1.Text+'",CPF="'+cpf+'",CNPJ="'+cnpj+'" '   ;
  SQL:=SQL+' WHERE FN="'+EdtCodigo.Text+'"'  ;
  Try
     B.execSQL(SQL);
     ShowMessage('Fornecedor atualizado com sucesso!') ;
  Except
     on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
  End;

  SQL:='SELECT F.*,E.RUA,E.NUMERO,E.COMPLEMENTO,E.BAIRRO,E.CIDADE,E.ESTADO,E.CEP  FROM FORNECEDOR F JOIN ENDERECO E WHERE F.END=E.EN'  ;
  T:=B.GetUniTable(SQL);
  ExecTableToGrid(T,GridAtualizar);
  GridAtualizar.FixedRows:=1
end;

procedure TTFornecedores.BtnInserirClick(Sender: TObject);
var
  cep,tel,cpf,cnpj:string;
  codend:integer;
begin

  if EdtCEP.EditText='_____-___' then  cep:=''
  else cep:=EdtCEP.Text;
  if EdtTelefone.EditText='(__)____-____' then tel:=''
  else tel:= EdtTelefone.EditText;
  if EdtCPF.EditText='___.___.___-__' then  cpf:=''
  else cpf:=EdtCPF.Text;
  if EdtCNPJ.EditText='__.___.___/____-__' then  cnpj:=''
  else cnpj:=EdtCNPJ.Text;

  SQL:='INSERT INTO ENDERECO VALUES(NULL,"'+EdtLog.Text+'","'+EdtNumero.Text+'","'+EdtComplemento.Text+'","'+EdtBairro.Text+'", ';
  SQL:=SQL+'"'+EdtCidade.Text+'","'+CmbEstado.Text+'","'+cep+'")';
  Try
     B.execSQL(SQL);
  Except
     on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
  End;

  SQL:='SELECT last_insert_rowid()';
  T:=B.GetUniTable(SQL);
  codend:=T.FieldAsInteger(0);

  SQL:='INSERT INTO FORNECEDOR VALUES (null, "'+EdtNome.Text+'","'+tel+'", ' ;
  SQL:=SQL+ '"'+EdtEmail.Text+'","'+IntToStr(codend)+'","'+cpf+'","'+cnpj+'");';
  Try
     B.execSQL(SQL);
     showmessage('Fornecedor cadastrado com sucesso!');
  Except
     on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
  End;


  SQL:='SELECT F.FNOME AS NOME,F.TEL AS TEL,F.EMAIL,F.CPF,F.CNPJ,E.RUA,E.NUMERO,E.COMPLEMENTO, ';
  SQL:=SQL+' E.BAIRRO,E.CIDADE,E.ESTADO,E.CEP ';
  SQL:=SQL+' FROM FORNECEDOR F JOIN ENDERECO E WHERE F.END=E.EN'  ;
  T:=B.GetUniTable(SQL);
  ExecTableToGrid(T,GridInserir); 
  GridInserir.FixedRows:=1;
  BtnCancelarClick(Sender)
end;



procedure TTFornecedores.EditarShow(Sender: TObject);
begin
  SQL:='SELECT F.*,E.RUA,E.NUMERO,E.COMPLEMENTO,E.BAIRRO,E.CIDADE,E.ESTADO,E.CEP  FROM FORNECEDOR F JOIN ENDERECO E WHERE F.END=E.EN'  ;
  T:=B.GetUniTable(SQL);
  ExecTableToGrid(T,GridAtualizar);
  GridAtualizar.FixedRows:=1;
  T.Free
end;

procedure TTFornecedores.FormShow(Sender: TObject);
begin
   PageControl1.ActivePageIndex:=0;
end;

procedure TTFornecedores.GridAtualizarSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
   if Arow =0 then  exit;
   EdtCodigo.Text:=GridAtualizar.Cells[0,Arow];
   EdtNome1.Text:= GridAtualizar.Cells[1,Arow];
   EdtTelefone1.Text:= GridAtualizar.Cells[2,Arow] ;
   EdtEmail1.Text:= GridAtualizar.Cells[3,Arow] ;
   EdtCPF1.Text:=  GridAtualizar.Cells[5,Arow] ;
   EdtCNPJ1.Text:=  GridAtualizar.Cells[6,Arow] ;
   EdtLog1.Text:= GridAtualizar.Cells[7,Arow];
   EdtNumero1.Text:=GridAtualizar.Cells[8,Arow];
   EdtComplemento1.Text:=GridAtualizar.Cells[9,Arow];
   EdtBairro1.Text:=GridAtualizar.Cells[10,Arow];
   EdtCidade1.Text:=GridAtualizar.Cells[11,Arow];
   CmbEstado1.Text:=GridAtualizar.Cells[12,Arow];
   EdtCEP1.Text:=GridAtualizar.Cells[13,Arow];
end;

procedure TTFornecedores.InserirShow(Sender: TObject);
begin
  SQL:='SELECT F.FNOME AS NOME,F.TEL AS TEL,F.EMAIL,F.CPF,F.CNPJ,E.RUA,E.NUMERO,E.COMPLEMENTO, ';
  SQL:=SQL+' E.BAIRRO,E.CIDADE,E.ESTADO,E.CEP ';
  SQL:=SQL+' FROM FORNECEDOR F JOIN ENDERECO E WHERE F.END=E.EN'  ;
  T:=B.GetUniTable(SQL);
  ExecTableToGrid(T,GridInserir); 
  GridInserir.FixedRows:=1;
  T.Free
end;

end.
