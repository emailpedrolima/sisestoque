unit UProdutos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, ComCtrls, ToolWin, ImgList,SQLiteTable3,SqlTabGrid,
  UDbMod, Buttons, ExtCtrls, Mask, Types, StrUtils;

type
  TTProdutos = class(TForm)
    PageControl1: TPageControl;
    Inserir: TTabSheet;
    Editar: TTabSheet;
    GridInserir: TStringGrid;
    GridAtualizar: TStringGrid;
    ScrollBox1: TScrollBox;
    Label1: TLabel;
    EdtNome: TEdit;
    Label2: TLabel;
    Label4: TLabel;
    EdtPCusto: TEdit;
    Label5: TLabel;
    EdtPVenda: TEdit;
    Label6: TLabel;
    EdtQntd: TEdit;
    Label7: TLabel;
    EdtCategoria: TEdit;
    Label8: TLabel;
    cmbFornecedor: TComboBox;
    BitConfirmar: TBitBtn;
    BitCancelar: TBitBtn;
    Splitter1: TSplitter;
    ScrollBox2: TScrollBox;
    Label9: TLabel;
    EdtNome1: TEdit;
    Label10: TLabel;
    Label12: TLabel;
    EdtPCusto1: TEdit;
    Label13: TLabel;
    EdtPVenda1: TEdit;
    Label14: TLabel;
    EdtQntd1: TEdit;
    Label15: TLabel;
    EdtCategoria1: TEdit;
    Label16: TLabel;
    cmbFornecedor1: TComboBox;
    Label17: TLabel;
    EdtCodigo: TEdit;
    BitAtualizar: TBitBtn;
    BitBtn1: TBitBtn;
    Splitter2: TSplitter;
    mmoDesc: TMemo;
    Label3: TLabel;
    EdtEmin: TEdit;
    Foto: TImage;
    Label11: TLabel;
    mmoDesc1: TMemo;
    Label18: TLabel;
    Foto1: TImage;
    BitBtn2: TBitBtn;
    Label19: TLabel;
    EdtEmin1: TEdit;
    procedure BitConfirmarClick(Sender: TObject);
    procedure BitCancelarClick(Sender: TObject);
    procedure cmbFornecedorChange(Sender: TObject);
    procedure InserirShow(Sender: TObject);
    procedure EditarShow(Sender: TObject);
    procedure GridAtualizarSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
    procedure BtnAtualizarClick(Sender: TObject);


  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TProdutos: TTProdutos;
  T:TSQLiteUniTable;
  SQL:string;

implementation

{$R *.dfm}

procedure TTProdutos.BtnAtualizarClick(Sender: TObject);

   

begin
    if (EdtNome1.Text='') or (mmoDesc1.Text='') or (EdtPCusto1.Text='') or
       (EdtPVenda1.Text='') or (EdtQntd1.Text='') or (EdtCategoria1.Text='')  then begin
        showmessage('Todos os campos devem ser preenchidos!');
        exit;
     end;

    SQL:='UPDATE PRODUTO SET PRNOME ="'+EdtNome1.Text+'", DESCRICAO = "'+mmoDesc1.Text+'",PRECOC = "'+EdtPCusto1.Text+'",PRECOV="'+EdtPVenda1.Text+'",ESTOQUEMIN="'+EdtEMin1.Text+'",CATEGORIA="'+EdtCategoria1.Text+'"';
    SQL:=SQL+'WHERE PRN="'+EdtCodigo.Text+'"'  ;
    B.execSQL(SQL); 

    showmessage('Produto atualizado com sucesso!');
    
    SQL:='SELECT PRN AS CODIGO,PRNOME AS NOME, DESCRICAO,PRECOC AS CUSTO, PRECOV AS VENDA,ESTOQUE AS ESTOQUE_INICIAL,ESTOQUEMIN AS QNTD_ATUAL,CATEGORIA FROM PRODUTO;'  ;
    T:=B.GetUniTable(SQL);
    ExecTableToGrid(T,GridAtualizar);
    GridAtualizar.FixedRows:=1;
    T.Free
end;



procedure TTProdutos.BitCancelarClick(Sender: TObject);
begin
    EdtNome.Clear;
    cmbFornecedor.Text:='';
    mmoDesc.Clear;
    EdtPCusto.Clear;
    EdtPVenda.Clear;
    EdtQntd.Clear;
    EdtCategoria.Clear;
    EdtEmin.Clear;
    Foto.Picture:=nil;
    EdtNome1.Clear;
    cmbFornecedor1.Text:='';
    mmoDesc1.Clear;
    EdtPCusto1.Clear;
    EdtPVenda1.Clear;
    EdtQntd1.Clear;
    EdtCategoria1.Clear;
    EdtEmin1.Clear;
    Foto1.Picture:=nil;
    EdtCodigo.Clear


end;

procedure TTProdutos.BitConfirmarClick(Sender: TObject);  
begin
    if (EdtNome.Text='') or (mmoDesc.Text='') or (EdtPCusto.Text='') or
       (EdtPVenda.Text='') or (EdtQntd.Text='') or (EdtCategoria.Text='') or
       (cmbFornecedor.Text='') then begin
        showmessage('Todos os campos devem ser preenchidos!');
        exit;
     end;

    SQL:='INSERT INTO PRODUTO VALUES (null,"'+EdtNome.Text+'","'+mmoDesc.Text+'",null,"'+EdtPCusto.Text+'","'+EdtPVenda.Text+'","'+EdtQntd.Text+'","'+EdtEmin.Text+'","'+EdtCategoria.Text+'",(SELECT FN FROM FORNECEDOR WHERE FNOME ="'+cmbFornecedor.Text+'"));';
    T:=B.GetUniTable(SQL);
    showmessage('Produto adicionado com sucesso!');
    BitCancelarClick(Sender)   ;
    SQL:='SELECT PRNOME AS NOME,DESCRICAO,PRECOC AS CUSTO, PRECOV AS VENDA,ESTOQUE ,ESTOQUEMIN AS ESTOQUE_MINIMO,CATEGORIA FROM PRODUTO';
    T:=B.GetUniTable(SQL);
    ExecTableToGrid(T,GridInserir);
    GridInserir.FixedRows:=1;
    T.Free;
end;



procedure TTProdutos.cmbFornecedorChange(Sender: TObject);
begin
  SQL:='SELECT FNOME FROM FORNECEDOR'; 
  T:=B.GetUniTable(SQL);
  While not T.EOF do begin
    cmbFornecedor.Items.Add(T.FieldAsString(0));
    cmbFornecedor1.Items.Add(T.FieldAsString(0));
    T.Next;
  end;
end;

procedure TTProdutos.InserirShow(Sender: TObject);
begin
  SQL:='SELECT PRNOME AS NOME,DESCRICAO,PRECOC AS CUSTO, PRECOV AS VENDA,ESTOQUE ,ESTOQUEMIN AS RESTANTE,CATEGORIA FROM PRODUTO';
  T:=B.GetUniTable(SQL);
  ExecTableToGrid(T,GridInserir);
  GridInserir.FixedRows:=1;
  T.Free
end;

procedure TTProdutos.EditarShow(Sender: TObject);
begin
  SQL:='SELECT PRN AS CODIGO,PRNOME AS NOME, DESCRICAO,PRECOC AS CUSTO, PRECOV AS VENDA,ESTOQUE AS ESTOQUE,ESTOQUEMIN AS ESTOQUE_MINIMO,CATEGORIA FROM PRODUTO;'  ;
  T:=B.GetUniTable(SQL);
  ExecTableToGrid(T,GridAtualizar);
  GridAtualizar.FixedRows:=1;
  T.Free
end;
procedure TTProdutos.GridAtualizarSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin

   if Arow =0 then  exit;
   EdtCodigo.Text:=GridAtualizar.Cells[0,Arow];   
   EdtNome1.Text:= GridAtualizar.Cells[1,Arow];
   mmoDesc1.Text:= GridAtualizar.Cells[2,Arow] ;
   EdtPCusto1.Text:= GridAtualizar.Cells[3,Arow] ;
   EdtPVenda1.Text:=  GridAtualizar.Cells[4,Arow] ;
   EdtQntd1.Text:=  GridAtualizar.Cells[5,Arow] ;
   EdtCategoria1.Text:= GridAtualizar.Cells[7,Arow];
   EdtEmin1.Text:= GridAtualizar.Cells[6,Arow]

end;
end.


