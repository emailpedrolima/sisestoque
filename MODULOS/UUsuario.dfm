object TUsuario: TTUsuario
  Left = 0
  Top = 0
  Caption = 'Usuario'
  ClientHeight = 380
  ClientWidth = 572
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 572
    Height = 380
    ActivePage = Inserir
    Align = alClient
    TabOrder = 0
    object Inserir: TTabSheet
      Caption = 'Inserir'
      OnShow = InserirShow
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 513
      ExplicitHeight = 306
      object Splitter1: TSplitter
        Left = 0
        Top = 161
        Width = 564
        Height = 3
        Cursor = crVSplit
        Align = alTop
        ExplicitWidth = 191
      end
      object GridInserir: TStringGrid
        Left = 0
        Top = 164
        Width = 564
        Height = 188
        Align = alClient
        FixedCols = 0
        TabOrder = 0
        ExplicitLeft = 3
        ExplicitTop = 208
        ExplicitWidth = 344
        ExplicitHeight = 129
      end
      object ScrollBox1: TScrollBox
        Left = 0
        Top = 0
        Width = 564
        Height = 161
        Align = alTop
        TabOrder = 1
        object Label1: TLabel
          Left = 32
          Top = 21
          Width = 27
          Height = 13
          Caption = 'Nome'
        end
        object Label2: TLabel
          Left = 32
          Top = 48
          Width = 20
          Height = 13
          Caption = 'Tipo'
        end
        object Label3: TLabel
          Left = 32
          Top = 80
          Width = 30
          Height = 13
          Caption = 'Senha'
        end
        object Label4: TLabel
          Left = 32
          Top = 112
          Width = 47
          Height = 13
          Caption = 'Confirmar'
        end
        object Edit1: TEdit
          Left = 96
          Top = 18
          Width = 121
          Height = 21
          TabOrder = 0
        end
        object ComboBox1: TComboBox
          Left = 96
          Top = 45
          Width = 121
          Height = 21
          ItemHeight = 13
          TabOrder = 1
          Items.Strings = (
            'ADM'
            'USU')
        end
        object Edit3: TEdit
          Left = 96
          Top = 77
          Width = 121
          Height = 21
          PasswordChar = '*'
          TabOrder = 2
        end
        object Edit2: TEdit
          Left = 96
          Top = 109
          Width = 121
          Height = 21
          PasswordChar = '*'
          TabOrder = 3
        end
        object BtnInserir: TBitBtn
          Left = 257
          Top = 28
          Width = 90
          Height = 25
          Caption = 'Inserir'
          Default = True
          TabOrder = 4
          OnClick = BtnInserirClick
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333333333333330000333333333333333333333333F33333333333
            00003333344333333333333333388F3333333333000033334224333333333333
            338338F3333333330000333422224333333333333833338F3333333300003342
            222224333333333383333338F3333333000034222A22224333333338F338F333
            8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
            33333338F83338F338F33333000033A33333A222433333338333338F338F3333
            0000333333333A222433333333333338F338F33300003333333333A222433333
            333333338F338F33000033333333333A222433333333333338F338F300003333
            33333333A222433333333333338F338F00003333333333333A22433333333333
            3338F38F000033333333333333A223333333333333338F830000333333333333
            333A333333333333333338330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
        end
        object BtnCancelar: TBitBtn
          Left = 257
          Top = 59
          Width = 90
          Height = 25
          Caption = 'Cancelar'
          TabOrder = 5
          OnClick = BtnCancelarClick
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333333333000033338833333333333333333F333333333333
            0000333911833333983333333388F333333F3333000033391118333911833333
            38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
            911118111118333338F3338F833338F3000033333911111111833333338F3338
            3333F8330000333333911111183333333338F333333F83330000333333311111
            8333333333338F3333383333000033333339111183333333333338F333833333
            00003333339111118333333333333833338F3333000033333911181118333333
            33338333338F333300003333911183911183333333383338F338F33300003333
            9118333911183333338F33838F338F33000033333913333391113333338FF833
            38F338F300003333333333333919333333388333338FFF830000333333333333
            3333333333333333333888330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
        end
      end
    end
    object Editar: TTabSheet
      Caption = 'Editar/Excluir'
      ImageIndex = 1
      OnShow = EditarShow
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 513
      ExplicitHeight = 306
      object Splitter2: TSplitter
        Left = 0
        Top = 153
        Width = 564
        Height = 3
        Cursor = crVSplit
        Align = alTop
        ExplicitTop = 177
        ExplicitWidth = 175
      end
      object GridAtualizar: TStringGrid
        Left = 0
        Top = 156
        Width = 564
        Height = 196
        Align = alClient
        FixedCols = 0
        TabOrder = 0
        OnSelectCell = GridAtualizarSelectCell
        ExplicitLeft = 19
        ExplicitTop = 208
        ExplicitWidth = 344
        ExplicitHeight = 129
      end
      object ScrollBox2: TScrollBox
        Left = 0
        Top = 0
        Width = 564
        Height = 153
        Align = alTop
        TabOrder = 1
        object Label5: TLabel
          Left = 32
          Top = 21
          Width = 27
          Height = 13
          Caption = 'Nome'
        end
        object Label6: TLabel
          Left = 32
          Top = 48
          Width = 20
          Height = 13
          Caption = 'Tipo'
        end
        object Label7: TLabel
          Left = 32
          Top = 80
          Width = 30
          Height = 13
          Caption = 'Senha'
        end
        object Label8: TLabel
          Left = 32
          Top = 112
          Width = 47
          Height = 13
          Caption = 'Confirmar'
        end
        object Label9: TLabel
          Left = 257
          Top = 112
          Width = 41
          Height = 13
          Caption = 'CODIGO'
        end
        object Edit4: TEdit
          Left = 96
          Top = 18
          Width = 121
          Height = 21
          TabOrder = 0
        end
        object ComboBox2: TComboBox
          Left = 96
          Top = 45
          Width = 121
          Height = 21
          ItemHeight = 13
          TabOrder = 1
          Items.Strings = (
            'ADM'
            'USU')
        end
        object Edit5: TEdit
          Left = 96
          Top = 77
          Width = 121
          Height = 21
          PasswordChar = '*'
          TabOrder = 2
        end
        object Edit6: TEdit
          Left = 96
          Top = 109
          Width = 121
          Height = 21
          PasswordChar = '*'
          TabOrder = 3
        end
        object BtnAtualizar: TBitBtn
          Left = 257
          Top = 28
          Width = 90
          Height = 25
          Caption = 'Atualizar'
          Default = True
          TabOrder = 4
          OnClick = BtnAtualizarClick
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333333333333330000333333333333333333333333F33333333333
            00003333344333333333333333388F3333333333000033334224333333333333
            338338F3333333330000333422224333333333333833338F3333333300003342
            222224333333333383333338F3333333000034222A22224333333338F338F333
            8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
            33333338F83338F338F33333000033A33333A222433333338333338F338F3333
            0000333333333A222433333333333338F338F33300003333333333A222433333
            333333338F338F33000033333333333A222433333333333338F338F300003333
            33333333A222433333333333338F338F00003333333333333A22433333333333
            3338F38F000033333333333333A223333333333333338F830000333333333333
            333A333333333333333338330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
        end
        object BtnExcluir: TBitBtn
          Left = 257
          Top = 59
          Width = 90
          Height = 25
          Cancel = True
          Caption = 'Exluir'
          TabOrder = 5
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333FFFFF333333000033333388888833333333333F888888FFF333
            000033338811111188333333338833FFF388FF33000033381119999111833333
            38F338888F338FF30000339119933331111833338F388333383338F300003391
            13333381111833338F8F3333833F38F3000039118333381119118338F38F3338
            33F8F38F000039183333811193918338F8F333833F838F8F0000391833381119
            33918338F8F33833F8338F8F000039183381119333918338F8F3833F83338F8F
            000039183811193333918338F8F833F83333838F000039118111933339118338
            F3833F83333833830000339111193333391833338F33F8333FF838F300003391
            11833338111833338F338FFFF883F83300003339111888811183333338FF3888
            83FF83330000333399111111993333333388FFFFFF8833330000333333999999
            3333333333338888883333330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
        end
        object EdtCodigo: TEdit
          Left = 304
          Top = 109
          Width = 43
          Height = 21
          Color = clInactiveCaption
          Enabled = False
          TabOrder = 6
        end
      end
    end
  end
end
