unit USenha;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, jpeg, ExtCtrls, sqlitetable3;

type
  TTSenha = class(TForm)
    Image1: TImage;
    LblUsuario: TLabel;
    EdtUsuario: TEdit;
    LblSenha: TLabel;
    EdtSenha: TEdit;
    Panel1: TPanel;
    BtnEntrar: TButton;
    BtnSair: TButton;
    procedure FormShow(Sender: TObject);
    procedure BtnSairClick(Sender: TObject);
    procedure BtnEntrarClick(Sender: TObject);
    procedure EdtUsuarioEnter(Sender: TObject);
    procedure EdtUsuarioExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TSenha: TTSenha;
  tpusuario,usuario,un :string;

implementation

 uses UDbmod, UPrincipal, UTrocaSenha;

{$R *.dfm}

 var
   Ts:tSQLITEUNITABLE;
   num_tentativas:integer;

function Rot13(senha:string):string;
var
   i:integer;
   c:char;
   resultado:string;
begin
   for i := 1 to Length(senha) do begin
       c:=senha[i];
       if(c>='A') and (c<='M') then c:=chr(ord(c)+13)
       else if (c>='N') and (c<='Z') then c:=chr(ord(c)-13);
       resultado:=resultado+c
   end;
   Rot13:=resultado
end;


procedure TTSenha.BtnEntrarClick(Sender: TObject);
var
   senha:string;
begin
   senha:=EdtSenha.Text;
   inc(num_tentativas);

   Ts:=B.GetUniTable('SELECT SENHA,TIPO,UN,TROCASENHA FROM USUARIO WHERE NOME="'+UpperCase(EdtUsuario.Text)+'" AND SENHA="'+Rot13(UpperCase(EdtSenha.Text))+'";');
   usuario:=UpperCase(EdtUsuario.Text);

   if not Ts.FieldIsNull(0) then begin
     ShowMessage('Login OK!');
     TSenha.Free;
     tpusuario:=Ts.FieldAsString(1);
     un:=Ts.FieldAsString(2);
     if Ts.FieldAsString(3)='SIM' then
        TTrocaSenha.ShowModal
     else
         TPrincipal.ShowModal
   end
   else begin
     ShowMessage('Senha inv�lida!');
     EdtSenha.Clear;
     EdtSenha.SetFocus
   end;
   if num_tentativas=3 then begin
     ShowMessage('Senha inv�lida! N�mero de tentativas excedido, o programa ser� encerrado!');
     Application.Terminate
   end;



end;

procedure TTSenha.BtnSairClick(Sender: TObject);
begin
   Application.Terminate;
end;

procedure TTSenha.EdtUsuarioEnter(Sender: TObject);
begin
   TEdit(Sender).Color:=clYellow
end;

procedure TTSenha.EdtUsuarioExit(Sender: TObject);
begin
   TEdit(Sender).Color:=clWhite
end;

procedure TTSenha.FormShow(Sender: TObject);
begin
   num_tentativas:=0;
   EdtUsuario.Clear;
   EdtSenha.Clear;
end;


end.
