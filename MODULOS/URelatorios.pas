unit URelatorios;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,

  SqlTabGrid,sqlitetable3, ExtCtrls, Grids, StdCtrls,Jpeg, Buttons;

type
  TTRelatorios = class(TForm)
    PanelFiltro: TPanel;
    Panel2: TPanel;
    GridRelat: TStringGrid;
    ImgProduto: TImage;
    MmoProduto: TMemo;
    ComboBox1: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    LblTitulo: TLabel;
    Edit1: TEdit;
    BitBtn1: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure GridRelatSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TRelatorios: TTRelatorios;
  T:  tSQLITEUNITABLE;

implementation

uses UPrincipal, UDbmod;

{$R *.dfm}

procedure TTRelatorios.BitBtn1Click(Sender: TObject);
begin
 if  ComboBox1.Text='' then ShowMessage('Selecione um m�s!');
     if Edit1.Text='' then  ShowMessage('Defina um ano!');

     if titulo='Lista de vendas por m�s' then begin
        SQL:='SELECT P.PN AS NUMERO_PEDIDO,P.VALOR,P.DATA FROM PEDIDO P WHERE P.STATUS="APROVADO" ';
        SQL:=SQL+'AND strftime("%m", P.DATA)="0'+IntToStr(ComboBox1.ItemIndex+1) +'" AND strftime("%Y", P.DATA)="'+Edit1.Text+'"';
        end
     else if titulo='Total de vendas por m�s' then  begin
        SQL:='SELECT SUM(VALOR) AS TOTAL_VENDAS FROM PEDIDO WHERE STATUS="APROVADO" AND strftime("%m", DATA)="0'+IntToStr(ComboBox1.ItemIndex+1) +'" '         ;
        SQL:=SQL+'AND strftime("%Y", DATA)="'+Edit1.Text+'"' 

     end;

     T:=B.GetUniTable(SQL);
     Exectabletogrid(T,GridRelat);
     if Not T.FieldIsNull(0) then  GridRelat.FixedRows:=1


end;

procedure TTRelatorios.FormShow(Sender: TObject);
begin

   TRelatorios.Caption:=titulo;
   LblTitulo.Caption:=subtitulo;
   PanelFiltro.Visible:=false;
   ImgProduto.Visible:=false;
   MmoProduto.Visible:=false;


   if (titulo='Lista de vendas por m�s') OR (titulo='Total de vendas por m�s')  then
       PanelFiltro.Visible:=true
   else begin
      T:=B.GetUniTable(SQL);
      Exectabletogrid(T,GridRelat);
      GridRelat.FixedRows:=1;
      end;
   end;
                      
procedure TTRelatorios.GridRelatSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
var
    primcol:string;
    ms: TMemoryStream;
    pic: TJPegImage;
    Taux: tsqliteUniTable;
    i:integer;
    temFoto,temDesc:Boolean;
begin   

    with T do begin

       temDesc:=false;  TemFoto:=false;

       for i := 0 to ColCount - 1 do begin
         if Uppercase(Columns[i])='DESCRICAO' then begin temDesc:=true; end;
         if Uppercase(Columns[i])='FOTO'  then begin temFoto:=true; end;
       end;
    end;

    MmoProduto.Visible:=false;
    ImgProduto.Visible:=false;


    if Arow =0 then  exit;

    primcol:=  GridRelat.Cells[0,Arow];

    if temDesc then
       try
         Taux:=B.GetUniTable('SELECT DESCRICAO FROM PRODUTO WHERE PRN='+primcol );
         with Taux do begin
           if FieldIsNull(0) then MmoProduto.Text:= 'Produto sem descri��o'
           else MmoProduto.Text:=  FieldAsBlobtext(0);
         end;
         MmoProduto.Visible:=true;
       except
         on e:exception do showmessage('Erro DESCRICAO:'+ copy(e.Message,pos('":',e.Message)+2));
     end;

     // Agora a picture (eh um proc similar)
     ImgProduto.Visible:=false;
     if temFoto then
        try
          Taux:=B.GetUniTable('SELECT FOTO FROM PRODUTO WHERE PRN='+primcol);
          with Taux do begin
             if FieldIsNull(0) then
                begin// nao tem nada
                   showmessage(' Produto sem foto cadastrada!');
                   ImgProduto.Visible:=false; // podia mostrar uma img nula
                end
             else
                begin  // pega a imagem que esta no blob
                   ms := FieldAsBlob(0);
                   ms.Position := 0;
                   pic := TJPEGImage.Create;
                   pic.LoadFromStream(ms);
                   ImgProduto.Picture.Graphic := pic;
                   ImgProduto.Visible:=true;
                   ImgProduto.stretch:=true;
                   pic.Free; // libera a pic
                end;
             end;
        except     // se der erro cai aqui na excecao
          on e:exception do showmessage('Erro FOTO:'+copy(e.Message,pos('":',e.Message)+2));
      end;

    end;
end.
