object TPedidos: TTPedidos
  Left = 0
  Top = 0
  Caption = 'P E D I D O S'
  ClientHeight = 511
  ClientWidth = 886
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 886
    Height = 511
    ActivePage = Editar
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 1014
    object Inserir: TTabSheet
      Caption = 'Inserir'
      OnShow = InserirShow
      ExplicitWidth = 1006
      object Splitter2: TSplitter
        Left = 0
        Top = 337
        Width = 878
        Height = 3
        Cursor = crVSplit
        Align = alTop
        ExplicitWidth = 146
      end
      object ScrollBox2: TScrollBox
        Left = 0
        Top = 0
        Width = 878
        Height = 337
        VertScrollBar.Range = 300
        Align = alTop
        AutoScroll = False
        TabOrder = 0
        ExplicitWidth = 1006
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 874
          Height = 333
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 1002
          object Label2: TLabel
            Left = 16
            Top = 16
            Width = 27
            Height = 13
            Caption = 'Data:'
          end
          object Label3: TLabel
            Left = 287
            Top = 16
            Width = 106
            Height = 13
            Caption = 'Forma de pagamento:'
          end
          object Label4: TLabel
            Left = 16
            Top = 56
            Width = 35
            Height = 13
            Caption = 'Status:'
          end
          object Label5: TLabel
            Left = 287
            Top = 56
            Width = 37
            Height = 13
            Caption = 'Cliente:'
          end
          object Label6: TLabel
            Left = 287
            Top = 103
            Width = 113
            Height = 13
            Caption = 'Desconto aplicado (%):'
          end
          object Label7: TLabel
            Left = 16
            Top = 103
            Width = 98
            Height = 13
            Caption = 'Valor do pedido, R$:'
          end
          object cmbPG: TComboBox
            Left = 399
            Top = 13
            Width = 145
            Height = 21
            ItemHeight = 13
            TabOrder = 0
            Items.Strings = (
              'BOLETO'
              'CARTAO'
              'TRANSFERENCIA')
          end
          object cmbStatus: TComboBox
            Left = 64
            Top = 53
            Width = 145
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clInactiveCaptionText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemHeight = 13
            ParentFont = False
            TabOrder = 1
            Text = 'EM PROCESSAMENTO'
            Items.Strings = (
              'APROVADO'
              'CANCELADO'
              'EM PROCESSAMENTO')
          end
          object cmbCliente: TComboBox
            Left = 330
            Top = 53
            Width = 145
            Height = 21
            ItemHeight = 13
            TabOrder = 2
          end
          object EdtDesconto: TEdit
            Left = 406
            Top = 100
            Width = 121
            Height = 21
            TabOrder = 3
          end
          object EdtValor: TEdit
            Left = 120
            Top = 100
            Width = 121
            Height = 21
            Color = clInactiveCaption
            Enabled = False
            TabOrder = 4
          end
          object GroupBox1: TGroupBox
            Left = 16
            Top = 135
            Width = 673
            Height = 186
            Caption = ' Itens  '
            TabOrder = 5
            object clItens: TCheckListBox
              Left = 24
              Top = 24
              Width = 201
              Height = 153
              ItemHeight = 13
              TabOrder = 0
            end
            object GridPedido: TStringGrid
              Left = 345
              Top = 24
              Width = 288
              Height = 153
              ColCount = 3
              FixedCols = 0
              RowCount = 2
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
              TabOrder = 1
            end
            object BitBtn1: TBitBtn
              Left = 248
              Top = 33
              Width = 75
              Height = 64
              Caption = '>>'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -24
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 2
              OnClick = BitBtn1Click
            end
            object BitBtn3: TBitBtn
              Left = 248
              Top = 103
              Width = 75
              Height = 64
              Caption = '<<'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -24
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 3
              OnClick = BitBtn3Click
            end
          end
          object Data: TDateTimePicker
            Left = 64
            Top = 13
            Width = 112
            Height = 21
            Date = 43206.611265625000000000
            Format = 'dd/MM/yyyy'
            Time = 43206.611265625000000000
            TabOrder = 6
          end
          object BtnCancelar: TBitBtn
            Left = 711
            Top = 236
            Width = 129
            Height = 77
            Cancel = True
            Caption = 'Cancelar'
            TabOrder = 7
            OnClick = BtnCancelarClick
            Glyph.Data = {
              DE010000424DDE01000000000000760000002800000024000000120000000100
              0400000000006801000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              333333333333333333333333000033338833333333333333333F333333333333
              0000333911833333983333333388F333333F3333000033391118333911833333
              38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
              911118111118333338F3338F833338F3000033333911111111833333338F3338
              3333F8330000333333911111183333333338F333333F83330000333333311111
              8333333333338F3333383333000033333339111183333333333338F333833333
              00003333339111118333333333333833338F3333000033333911181118333333
              33338333338F333300003333911183911183333333383338F338F33300003333
              9118333911183333338F33838F338F33000033333913333391113333338FF833
              38F338F300003333333333333919333333388333338FFF830000333333333333
              3333333333333333333888330000333333333333333333333333333333333333
              0000}
            NumGlyphs = 2
          end
        end
      end
      object BtnGeraPedido: TBitBtn
        Left = 711
        Top = 153
        Width = 129
        Height = 77
        Caption = 'Criar Pedido'
        Default = True
        TabOrder = 1
        OnClick = BtnGeraPedidoClick
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333330000333333333333333333333333F33333333333
          00003333344333333333333333388F3333333333000033334224333333333333
          338338F3333333330000333422224333333333333833338F3333333300003342
          222224333333333383333338F3333333000034222A22224333333338F338F333
          8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
          33333338F83338F338F33333000033A33333A222433333338333338F338F3333
          0000333333333A222433333333333338F338F33300003333333333A222433333
          333333338F338F33000033333333333A222433333333333338F338F300003333
          33333333A222433333333333338F338F00003333333333333A22433333333333
          3338F38F000033333333333333A223333333333333338F830000333333333333
          333A333333333333333338330000333333333333333333333333333333333333
          0000}
        NumGlyphs = 2
      end
      object GridInserir: TStringGrid
        Left = 0
        Top = 340
        Width = 878
        Height = 143
        Align = alClient
        FixedCols = 0
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 2
        ExplicitWidth = 1006
        RowHeights = (
          24
          24
          24
          24
          24)
      end
    end
    object Editar: TTabSheet
      Caption = 'Editar/Excluir'
      ImageIndex = 1
      OnShow = EditarShow
      ExplicitWidth = 1006
      object Splitter1: TSplitter
        Left = 0
        Top = 329
        Width = 878
        Height = 3
        Cursor = crVSplit
        Align = alTop
        ExplicitWidth = 154
      end
      object ScrollBox1: TScrollBox
        Left = 0
        Top = 0
        Width = 878
        Height = 329
        HorzScrollBar.Range = 689
        VertScrollBar.Range = 350
        Align = alTop
        AutoScroll = False
        TabOrder = 0
        ExplicitWidth = 1006
        object Label15: TLabel
          Left = 24
          Top = 16
          Width = 91
          Height = 13
          Caption = 'N'#250'mero do pedido:'
        end
        object Label16: TLabel
          Left = 327
          Top = 16
          Width = 27
          Height = 13
          Caption = 'Data:'
        end
        object Label17: TLabel
          Left = 520
          Top = 16
          Width = 106
          Height = 13
          Caption = 'Forma de pagamento:'
        end
        object Label18: TLabel
          Left = 24
          Top = 50
          Width = 35
          Height = 13
          Caption = 'Status:'
        end
        object Label19: TLabel
          Left = 310
          Top = 50
          Width = 37
          Height = 13
          Caption = 'Cliente:'
        end
        object Label20: TLabel
          Left = 310
          Top = 92
          Width = 113
          Height = 13
          Caption = 'Desconto aplicado (%):'
        end
        object Label21: TLabel
          Left = 24
          Top = 92
          Width = 98
          Height = 13
          Caption = 'Valor do pedido, R$:'
        end
        object EdtPedido1: TEdit
          Left = 121
          Top = 13
          Width = 121
          Height = 21
          Color = clInactiveCaption
          Enabled = False
          TabOrder = 0
        end
        object Data1: TDateTimePicker
          Left = 360
          Top = 13
          Width = 112
          Height = 21
          Date = 43206.611265625000000000
          Format = 'dd/MM/yyyy'
          Time = 43206.611265625000000000
          TabOrder = 1
        end
        object cmbPG1: TComboBox
          Left = 632
          Top = 13
          Width = 145
          Height = 21
          ItemHeight = 13
          TabOrder = 2
          Items.Strings = (
            'BOLETO'
            'CARTAO'
            'TRANSFERENCIA')
        end
        object cmbStatus1: TComboBox
          Left = 65
          Top = 47
          Width = 145
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clInactiveCaptionText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          TabOrder = 3
          Items.Strings = (
            'APROVADO'
            'CANCELADO'
            'EM PROCESSAMENTO')
        end
        object cmbCliente1: TComboBox
          Left = 353
          Top = 47
          Width = 145
          Height = 21
          ItemHeight = 13
          TabOrder = 4
        end
        object EdtDesconto1: TEdit
          Left = 429
          Top = 89
          Width = 121
          Height = 21
          TabOrder = 5
        end
        object EdtValor1: TEdit
          Left = 128
          Top = 89
          Width = 121
          Height = 21
          Color = clInactiveCaption
          Enabled = False
          TabOrder = 6
        end
        object GroupBox2: TGroupBox
          Left = 24
          Top = 130
          Width = 665
          Height = 186
          Caption = ' Itens '
          TabOrder = 7
          object clItens1: TCheckListBox
            Left = 24
            Top = 24
            Width = 201
            Height = 153
            ItemHeight = 13
            TabOrder = 0
          end
          object BtnAdd1: TButton
            Left = 248
            Top = 55
            Width = 75
            Height = 25
            Caption = '>>'
            TabOrder = 1
            OnClick = BtnAdd1Click
          end
          object Button4: TButton
            Left = 248
            Top = 103
            Width = 75
            Height = 25
            Caption = '<<'
            TabOrder = 2
            Visible = False
          end
          object GridPedido1: TStringGrid
            Left = 345
            Top = 24
            Width = 288
            Height = 153
            ColCount = 3
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
            TabOrder = 3
          end
        end
        object BtnAtualizar: TBitBtn
          Left = 707
          Top = 138
          Width = 136
          Height = 49
          Caption = 'Atualizar Pedido'
          Default = True
          TabOrder = 8
          OnClick = BtnAtualizarClick
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333333333333330000333333333333333333333333F33333333333
            00003333344333333333333333388F3333333333000033334224333333333333
            338338F3333333330000333422224333333333333833338F3333333300003342
            222224333333333383333338F3333333000034222A22224333333338F338F333
            8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
            33333338F83338F338F33333000033A33333A222433333338333338F338F3333
            0000333333333A222433333333333338F338F33300003333333333A222433333
            333333338F338F33000033333333333A222433333333333338F338F300003333
            33333333A222433333333333338F338F00003333333333333A22433333333333
            3338F38F000033333333333333A223333333333333338F830000333333333333
            333A333333333333333338330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
        end
        object BitBtn2: TBitBtn
          Left = 707
          Top = 193
          Width = 136
          Height = 49
          Cancel = True
          Caption = 'Cancelar'
          TabOrder = 9
          OnClick = BtnCancelarClick
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333333333000033338833333333333333333F333333333333
            0000333911833333983333333388F333333F3333000033391118333911833333
            38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
            911118111118333338F3338F833338F3000033333911111111833333338F3338
            3333F8330000333333911111183333333338F333333F83330000333333311111
            8333333333338F3333383333000033333339111183333333333338F333833333
            00003333339111118333333333333833338F3333000033333911181118333333
            33338333338F333300003333911183911183333333383338F338F33300003333
            9118333911183333338F33838F338F33000033333913333391113333338FF833
            38F338F300003333333333333919333333388333338FFF830000333333333333
            3333333333333333333888330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
        end
        object BtnExcluir: TBitBtn
          Left = 707
          Top = 248
          Width = 136
          Height = 49
          Cancel = True
          Caption = 'Excluir Pedido'
          TabOrder = 10
          OnClick = BtnExcluirClick
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333FFFFF333333000033333388888833333333333F888888FFF333
            000033338811111188333333338833FFF388FF33000033381119999111833333
            38F338888F338FF30000339119933331111833338F388333383338F300003391
            13333381111833338F8F3333833F38F3000039118333381119118338F38F3338
            33F8F38F000039183333811193918338F8F333833F838F8F0000391833381119
            33918338F8F33833F8338F8F000039183381119333918338F8F3833F83338F8F
            000039183811193333918338F8F833F83333838F000039118111933339118338
            F3833F83333833830000339111193333391833338F33F8333FF838F300003391
            11833338111833338F338FFFF883F83300003339111888811183333338FF3888
            83FF83330000333399111111993333333388FFFFFF8833330000333333999999
            3333333333338888883333330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
        end
      end
      object GridAtualizar: TStringGrid
        Left = 0
        Top = 332
        Width = 878
        Height = 151
        Align = alClient
        FixedCols = 0
        TabOrder = 1
        OnSelectCell = GridAtualizarSelectCell
        ExplicitWidth = 1006
      end
    end
  end
end
