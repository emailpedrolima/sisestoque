object TProdutos: TTProdutos
  Left = 0
  Top = 0
  Caption = 'Estoque'
  ClientHeight = 560
  ClientWidth = 591
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 591
    Height = 560
    ActivePage = Inserir
    Align = alClient
    TabOrder = 0
    ExplicitLeft = 1
    ExplicitTop = 5
    ExplicitWidth = 602
    ExplicitHeight = 572
    object Inserir: TTabSheet
      Caption = 'Inserir'
      OnShow = InserirShow
      ExplicitWidth = 594
      ExplicitHeight = 544
      object Splitter1: TSplitter
        Left = 0
        Top = 329
        Width = 583
        Height = 3
        Cursor = crVSplit
        Align = alTop
        ExplicitTop = 273
        ExplicitWidth = 271
      end
      object GridInserir: TStringGrid
        Left = 0
        Top = 332
        Width = 583
        Height = 200
        Align = alClient
        ColCount = 7
        FixedCols = 0
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 0
        ExplicitTop = 308
        ExplicitWidth = 594
        ExplicitHeight = 236
      end
      object ScrollBox1: TScrollBox
        Left = 0
        Top = 0
        Width = 583
        Height = 329
        Align = alTop
        TabOrder = 1
        ExplicitWidth = 594
        object Label1: TLabel
          Left = 16
          Top = 35
          Width = 87
          Height = 13
          Caption = 'Nome do produto:'
        end
        object Label4: TLabel
          Left = 16
          Top = 104
          Width = 99
          Height = 13
          Caption = 'Pre'#231'o de custo (R$):'
        end
        object Label5: TLabel
          Left = 288
          Top = 104
          Width = 103
          Height = 13
          Caption = 'Pre'#231'o de venda (R$):'
        end
        object Label2: TLabel
          Left = 21
          Top = 182
          Width = 50
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label8: TLabel
          Left = 304
          Top = 35
          Width = 59
          Height = 13
          Caption = 'Fornecedor:'
        end
        object Label6: TLabel
          Left = 16
          Top = 139
          Width = 119
          Height = 13
          Caption = 'Quantidade em estoque:'
        end
        object Label7: TLabel
          Left = 16
          Top = 72
          Width = 51
          Height = 13
          Caption = 'Categoria:'
        end
        object Label18: TLabel
          Left = 304
          Top = 72
          Width = 78
          Height = 13
          Caption = 'Estoque m'#237'nimo:'
        end
        object Foto: TImage
          Left = 369
          Top = 139
          Width = 185
          Height = 134
          Stretch = True
        end
        object Label3: TLabel
          Left = 323
          Top = 139
          Width = 26
          Height = 13
          Caption = 'Foto:'
        end
        object EdtNome: TEdit
          Left = 109
          Top = 32
          Width = 156
          Height = 21
          TabOrder = 0
        end
        object EdtPCusto: TEdit
          Left = 121
          Top = 101
          Width = 121
          Height = 21
          TabOrder = 1
        end
        object EdtPVenda: TEdit
          Left = 397
          Top = 101
          Width = 121
          Height = 21
          TabOrder = 2
        end
        object cmbFornecedor: TComboBox
          Left = 369
          Top = 32
          Width = 145
          Height = 21
          ItemHeight = 13
          TabOrder = 3
        end
        object EdtQuant: TEdit
          Left = 141
          Top = 136
          Width = 121
          Height = 21
          TabOrder = 4
        end
        object cmbCategoria: TComboBox
          Left = 73
          Top = 69
          Width = 145
          Height = 21
          ItemHeight = 13
          TabOrder = 5
          Items.Strings = (
            'ACESSORIOS'
            'CANECAS'
            'COZINHA'
            'DECORACAO'
            'ESCRITORIO'
            'INFORMATICA'
            'OUTROS'
            'PAPELARIA'
            'VESTUARIO')
        end
        object BitConfirmar: TBitBtn
          Left = 89
          Top = 290
          Width = 121
          Height = 25
          Caption = 'Inserir'
          Default = True
          TabOrder = 6
          OnClick = BitConfirmarClick
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333333333333330000333333333333333333333333F33333333333
            00003333344333333333333333388F3333333333000033334224333333333333
            338338F3333333330000333422224333333333333833338F3333333300003342
            222224333333333383333338F3333333000034222A22224333333338F338F333
            8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
            33333338F83338F338F33333000033A33333A222433333338333338F338F3333
            0000333333333A222433333333333338F338F33300003333333333A222433333
            333333338F338F33000033333333333A222433333333333338F338F300003333
            33333333A222433333333333338F338F00003333333333333A22433333333333
            3338F38F000033333333333333A223333333333333338F830000333333333333
            333A333333333333333338330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
        end
        object BitCancelar: TBitBtn
          Left = 323
          Top = 290
          Width = 121
          Height = 25
          Caption = 'Cancelar'
          TabOrder = 7
          OnClick = BitCancelarClick
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333333333000033338833333333333333333F333333333333
            0000333911833333983333333388F333333F3333000033391118333911833333
            38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
            911118111118333338F3338F833338F3000033333911111111833333338F3338
            3333F8330000333333911111183333333338F333333F83330000333333311111
            8333333333338F3333383333000033333339111183333333333338F333833333
            00003333339111118333333333333833338F3333000033333911181118333333
            33338333338F333300003333911183911183333333383338F338F33300003333
            9118333911183333338F33838F338F33000033333913333391113333338FF833
            38F338F300003333333333333919333333388333338FFF830000333333333333
            3333333333333333333888330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
        end
        object EdtEmin: TEdit
          Left = 388
          Top = 66
          Width = 121
          Height = 21
          TabOrder = 8
        end
        object MmoDesc: TMemo
          Left = 89
          Top = 179
          Width = 185
          Height = 105
          Lines.Strings = (
            '')
          TabOrder = 9
        end
      end
    end
    object Editar: TTabSheet
      Caption = 'Editar/Excluir'
      ImageIndex = 1
      OnShow = EditarShow
      ExplicitWidth = 594
      ExplicitHeight = 544
      object Label9: TLabel
        Left = 24
        Top = 32
        Width = 27
        Height = 13
        Caption = 'Nome'
      end
      object Label10: TLabel
        Left = 24
        Top = 72
        Width = 46
        Height = 13
        Caption = 'Descri'#231#227'o'
      end
      object Label11: TLabel
        Left = 455
        Top = 72
        Width = 22
        Height = 13
        Caption = 'Foto'
      end
      object Label12: TLabel
        Left = 24
        Top = 112
        Width = 73
        Height = 13
        Caption = 'Pre'#231'o de Custo'
      end
      object Label13: TLabel
        Left = 175
        Top = 112
        Width = 75
        Height = 13
        Caption = 'Pre'#231'o de Venda'
      end
      object Label14: TLabel
        Left = 24
        Top = 144
        Width = 56
        Height = 13
        Caption = 'Quantidade'
      end
      object Label15: TLabel
        Left = 175
        Top = 144
        Width = 47
        Height = 13
        Caption = 'Categoria'
      end
      object Label16: TLabel
        Left = 24
        Top = 181
        Width = 55
        Height = 13
        Caption = 'Fornecedor'
      end
      object Label17: TLabel
        Left = 248
        Top = 181
        Width = 33
        Height = 13
        Caption = 'C'#243'digo'
      end
      object EdtNome1: TEdit
        Left = 72
        Top = 29
        Width = 265
        Height = 21
        TabOrder = 0
      end
      object EdtDescricao1: TEdit
        Left = 72
        Top = 69
        Width = 265
        Height = 21
        TabOrder = 1
      end
      object EdtPCusto1: TEdit
        Left = 103
        Top = 109
        Width = 66
        Height = 21
        TabOrder = 2
      end
      object EdtPVenda1: TEdit
        Left = 264
        Top = 109
        Width = 73
        Height = 21
        TabOrder = 3
      end
      object EdtQntd1: TEdit
        Left = 103
        Top = 136
        Width = 66
        Height = 21
        TabOrder = 4
      end
      object EdtCategoria1: TEdit
        Left = 264
        Top = 141
        Width = 73
        Height = 21
        TabOrder = 5
      end
      object BitBtn1: TBitBtn
        Left = 495
        Top = 169
        Width = 90
        Height = 25
        Caption = 'Cancelar'
        TabOrder = 6
        OnClick = BitCancelarClick
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333333333000033338833333333333333333F333333333333
          0000333911833333983333333388F333333F3333000033391118333911833333
          38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
          911118111118333338F3338F833338F3000033333911111111833333338F3338
          3333F8330000333333911111183333333338F333333F83330000333333311111
          8333333333338F3333383333000033333339111183333333333338F333833333
          00003333339111118333333333333833338F3333000033333911181118333333
          33338333338F333300003333911183911183333333383338F338F33300003333
          9118333911183333338F33838F338F33000033333913333391113333338FF833
          38F338F300003333333333333919333333388333338FFF830000333333333333
          3333333333333333333888330000333333333333333333333333333333333333
          0000}
        NumGlyphs = 2
      end
      object BitAtualizar: TBitBtn
        Left = 375
        Top = 169
        Width = 89
        Height = 25
        Caption = 'Atualizar'
        Default = True
        TabOrder = 7
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333330000333333333333333333333333F33333333333
          00003333344333333333333333388F3333333333000033334224333333333333
          338338F3333333330000333422224333333333333833338F3333333300003342
          222224333333333383333338F3333333000034222A22224333333338F338F333
          8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
          33333338F83338F338F33333000033A33333A222433333338333338F338F3333
          0000333333333A222433333333333338F338F33300003333333333A222433333
          333333338F338F33000033333333333A222433333333333338F338F300003333
          33333333A222433333333333338F338F00003333333333333A22433333333333
          3338F38F000033333333333333A223333333333333338F830000333333333333
          333A333333333333333338330000333333333333333333333333333333333333
          0000}
        NumGlyphs = 2
      end
      object GridAtualizar: TStringGrid
        Left = 24
        Top = 224
        Width = 537
        Height = 281
        FixedCols = 0
        TabOrder = 8
        OnSelectCell = GridAtualizarSelectCell
      end
      object ComboBox1: TComboBox
        Left = 103
        Top = 178
        Width = 121
        Height = 21
        ItemHeight = 13
        TabOrder = 9
      end
      object EdtCodigo: TEdit
        Left = 302
        Top = 178
        Width = 35
        Height = 21
        Color = clInactiveCaption
        Enabled = False
        TabOrder = 10
      end
    end
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Filter = 
      'All (*.jpg;*.jpeg)|*.jpg;*.jpeg|JPEG Image File (*.jpg)|*.jpg|JP' +
      'EG Image File (*.jpeg)|*.jpeg'
    Left = 536
    Top = 40
  end
end
