object TClientes: TTClientes
  Left = 0
  Top = 0
  Caption = 'Clientes'
  ClientHeight = 516
  ClientWidth = 692
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 692
    Height = 516
    ActivePage = Editar
    Align = alClient
    TabOrder = 0
    object Inserir: TTabSheet
      Caption = 'Inserir'
      ImageIndex = 1
      OnShow = InserirShow
      object Splitter1: TSplitter
        Left = 0
        Top = 317
        Width = 684
        Height = 3
        Cursor = crVSplit
        Align = alBottom
        ExplicitTop = 0
        ExplicitWidth = 336
      end
      object Panel1: TPanel
        Left = 0
        Top = 320
        Width = 684
        Height = 168
        Align = alBottom
        TabOrder = 0
        object GridInserir: TStringGrid
          Left = 1
          Top = 1
          Width = 682
          Height = 166
          Align = alClient
          FixedCols = 0
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
          TabOrder = 0
          ColWidths = (
            109
            134
            117
            128
            111)
        end
      end
      object ScrollBox1: TScrollBox
        Left = 0
        Top = 0
        Width = 684
        Height = 317
        VertScrollBar.Range = 330
        VertScrollBar.Tracking = True
        Align = alClient
        AutoScroll = False
        TabOrder = 1
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 663
          Height = 330
          Align = alClient
          TabOrder = 0
          object No: TLabel
            Left = 29
            Top = 24
            Width = 31
            Height = 13
            Caption = 'Nome:'
          end
          object Label2: TLabel
            Left = 29
            Top = 200
            Width = 46
            Height = 13
            Caption = 'Telefone:'
          end
          object Label3: TLabel
            Left = 29
            Top = 240
            Width = 32
            Height = 13
            Caption = 'E-mail:'
          end
          object Label21: TLabel
            Left = 393
            Top = 240
            Width = 23
            Height = 13
            Caption = 'CPF:'
          end
          object Label22: TLabel
            Left = 393
            Top = 200
            Width = 100
            Height = 13
            Caption = 'Data de Nascimento:'
          end
          object EdtNome: TEdit
            Left = 80
            Top = 21
            Width = 263
            Height = 21
            TabOrder = 0
          end
          object GroupBox1: TGroupBox
            Left = 29
            Top = 48
            Width = 623
            Height = 137
            Caption = 'Endere'#231'o'
            TabOrder = 1
            object Label9: TLabel
              Left = 27
              Top = 27
              Width = 59
              Height = 13
              Caption = 'Logradouro:'
            end
            object Label10: TLabel
              Left = 387
              Top = 27
              Width = 41
              Height = 13
              Caption = 'N'#250'mero:'
            end
            object Label11: TLabel
              Left = 311
              Top = 62
              Width = 32
              Height = 13
              Caption = 'Bairro:'
            end
            object Label13: TLabel
              Left = 450
              Top = 100
              Width = 23
              Height = 13
              Caption = 'CEP:'
            end
            object Label12: TLabel
              Left = 27
              Top = 100
              Width = 37
              Height = 13
              Caption = 'Cidade:'
            end
            object Label14: TLabel
              Left = 250
              Top = 100
              Width = 37
              Height = 13
              Caption = 'Estado:'
            end
            object Label5: TLabel
              Left = 27
              Top = 62
              Width = 69
              Height = 13
              Caption = 'Complemento:'
            end
            object EdtLog: TEdit
              Left = 92
              Top = 24
              Width = 260
              Height = 21
              TabOrder = 0
            end
            object EdtNumero: TEdit
              Left = 442
              Top = 24
              Width = 81
              Height = 21
              TabOrder = 1
            end
            object EdtBairro: TEdit
              Left = 349
              Top = 62
              Width = 151
              Height = 21
              TabOrder = 3
            end
            object EdtCEP: TMaskEdit
              Left = 479
              Top = 100
              Width = 120
              Height = 21
              EditMask = '00000\-999;1;_'
              MaxLength = 9
              TabOrder = 6
              Text = '     -   '
            end
            object EdtCidade: TEdit
              Left = 70
              Top = 100
              Width = 121
              Height = 21
              TabOrder = 4
            end
            object CmbEstado: TComboBox
              Left = 293
              Top = 100
              Width = 94
              Height = 21
              ItemHeight = 13
              TabOrder = 5
              Items.Strings = (
                'AC'
                'AL'
                'AP'
                'AM'
                'BA'
                'CE'
                'DF'
                'ES'
                'GO'
                'MA'
                'MT'
                'MS'
                'MG'
                'PA'
                'PB'
                'PR'
                'PE'
                'PI'
                'RJ'
                'RN'
                'RS'
                'RO'
                'RR'
                'SC'
                'SP'
                'SE'
                'TO')
            end
            object EdtComplemento: TEdit
              Left = 102
              Top = 62
              Width = 121
              Height = 21
              TabOrder = 2
            end
          end
          object EdtTelefone: TMaskEdit
            Left = 81
            Top = 197
            Width = 115
            Height = 21
            EditMask = '!\(99\)0000-0000;1;_'
            MaxLength = 13
            TabOrder = 2
            Text = '(  )    -    '
          end
          object EdtEmail: TEdit
            Left = 80
            Top = 237
            Width = 261
            Height = 21
            TabOrder = 4
          end
          object BtnInserir: TBitBtn
            Left = 81
            Top = 281
            Width = 121
            Height = 25
            Caption = 'Inserir'
            Default = True
            TabOrder = 6
            OnClick = BtnInserirClick
            Glyph.Data = {
              DE010000424DDE01000000000000760000002800000024000000120000000100
              0400000000006801000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              3333333333333333333333330000333333333333333333333333F33333333333
              00003333344333333333333333388F3333333333000033334224333333333333
              338338F3333333330000333422224333333333333833338F3333333300003342
              222224333333333383333338F3333333000034222A22224333333338F338F333
              8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
              33333338F83338F338F33333000033A33333A222433333338333338F338F3333
              0000333333333A222433333333333338F338F33300003333333333A222433333
              333333338F338F33000033333333333A222433333333333338F338F300003333
              33333333A222433333333333338F338F00003333333333333A22433333333333
              3338F38F000033333333333333A223333333333333338F830000333333333333
              333A333333333333333338330000333333333333333333333333333333333333
              0000}
            NumGlyphs = 2
          end
          object BtnCancelar: TBitBtn
            Left = 312
            Top = 281
            Width = 121
            Height = 25
            Caption = 'Cancelar'
            TabOrder = 7
            OnClick = BtnCancelarClick
            Glyph.Data = {
              DE010000424DDE01000000000000760000002800000024000000120000000100
              0400000000006801000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              333333333333333333333333000033338833333333333333333F333333333333
              0000333911833333983333333388F333333F3333000033391118333911833333
              38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
              911118111118333338F3338F833338F3000033333911111111833333338F3338
              3333F8330000333333911111183333333338F333333F83330000333333311111
              8333333333338F3333383333000033333339111183333333333338F333833333
              00003333339111118333333333333833338F3333000033333911181118333333
              33338333338F333300003333911183911183333333383338F338F33300003333
              9118333911183333338F33838F338F33000033333913333391113333338FF833
              38F338F300003333333333333919333333388333338FFF830000333333333333
              3333333333333333333888330000333333333333333333333333333333333333
              0000}
            NumGlyphs = 2
          end
          object EdtCPF: TMaskEdit
            Left = 422
            Top = 237
            Width = 120
            Height = 21
            EditMask = '000\.000\.000\-00;1;_'
            MaxLength = 14
            TabOrder = 5
            Text = '   .   .   -  '
          end
          object EdtDataN: TMaskEdit
            Left = 499
            Top = 197
            Width = 118
            Height = 21
            EditMask = '!99/99/0000;1;_'
            MaxLength = 10
            TabOrder = 3
            Text = '  /  /    '
          end
        end
      end
    end
    object Editar: TTabSheet
      Caption = 'Editar/Excluir'
      ImageIndex = 1
      OnShow = EditarShow
      object Splitter2: TSplitter
        Left = 0
        Top = 308
        Width = 684
        Height = 2
        Cursor = crVSplit
        Align = alBottom
        ExplicitTop = 302
      end
      object Panel2: TPanel
        Left = 0
        Top = 310
        Width = 684
        Height = 178
        Align = alBottom
        TabOrder = 0
        object GridAtualizar: TStringGrid
          Left = 1
          Top = 1
          Width = 682
          Height = 176
          Align = alClient
          FixedCols = 0
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSizing, goColSizing]
          TabOrder = 0
          OnSelectCell = GridAtualizarSelectCell
          ColWidths = (
            104
            113
            146
            112
            105)
        end
      end
      object ScrollBox2: TScrollBox
        Left = 0
        Top = 0
        Width = 684
        Height = 308
        HorzScrollBar.Range = 641
        VertScrollBar.Range = 330
        VertScrollBar.Tracking = True
        Align = alClient
        AutoScroll = False
        TabOrder = 1
        object Label4: TLabel
          Left = 26
          Top = 19
          Width = 31
          Height = 13
          Caption = 'Nome:'
        end
        object Label8: TLabel
          Left = 441
          Top = 19
          Width = 37
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label6: TLabel
          Left = 26
          Top = 202
          Width = 46
          Height = 13
          Caption = 'Telefone:'
        end
        object Label7: TLabel
          Left = 26
          Top = 240
          Width = 32
          Height = 13
          Caption = 'E-mail:'
        end
        object Label23: TLabel
          Left = 393
          Top = 240
          Width = 23
          Height = 13
          Caption = 'CPF:'
        end
        object Label24: TLabel
          Left = 378
          Top = 202
          Width = 100
          Height = 13
          Caption = 'Data de Nascimento:'
        end
        object EdtNome1: TEdit
          Left = 84
          Top = 16
          Width = 258
          Height = 21
          TabOrder = 0
        end
        object EdtCodigo: TEdit
          Left = 497
          Top = 16
          Width = 75
          Height = 21
          Color = clInactiveCaption
          Enabled = False
          TabOrder = 1
        end
        object EdtTelefone1: TMaskEdit
          Left = 90
          Top = 199
          Width = 115
          Height = 21
          EditMask = '!\(99\)0000-0000;1;_'
          MaxLength = 13
          TabOrder = 3
          Text = '(  )    -    '
        end
        object EdtEmail1: TEdit
          Left = 90
          Top = 237
          Width = 259
          Height = 21
          TabOrder = 5
        end
        object BtnAtualizar: TBitBtn
          Left = 84
          Top = 280
          Width = 121
          Height = 25
          Caption = 'Atualizar'
          Default = True
          TabOrder = 7
          OnClick = BtnAtualizarClick
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333333333333330000333333333333333333333333F33333333333
            00003333344333333333333333388F3333333333000033334224333333333333
            338338F3333333330000333422224333333333333833338F3333333300003342
            222224333333333383333338F3333333000034222A22224333333338F338F333
            8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
            33333338F83338F338F33333000033A33333A222433333338333338F338F3333
            0000333333333A222433333333333338F338F33300003333333333A222433333
            333333338F338F33000033333333333A222433333333333338F338F300003333
            33333333A222433333333333338F338F00003333333333333A22433333333333
            3338F38F000033333333333333A223333333333333338F830000333333333333
            333A333333333333333338330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
        end
        object BtnCancelar1: TBitBtn
          Left = 295
          Top = 280
          Width = 121
          Height = 25
          Cancel = True
          Caption = 'Cancelar'
          TabOrder = 8
          OnClick = BtnCancelarClick
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333333333000033338833333333333333333F333333333333
            0000333911833333983333333388F333333F3333000033391118333911833333
            38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
            911118111118333338F3338F833338F3000033333911111111833333338F3338
            3333F8330000333333911111183333333338F333333F83330000333333311111
            8333333333338F3333383333000033333339111183333333333338F333833333
            00003333339111118333333333333833338F3333000033333911181118333333
            33338333338F333300003333911183911183333333383338F338F33300003333
            9118333911183333338F33838F338F33000033333913333391113333338FF833
            38F338F300003333333333333919333333388333338FFF830000333333333333
            3333333333333333333888330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
        end
        object GroupBox2: TGroupBox
          Left = 26
          Top = 43
          Width = 623
          Height = 137
          Caption = ' Endere'#231'o '
          TabOrder = 2
          object Label1: TLabel
            Left = 27
            Top = 27
            Width = 59
            Height = 13
            Caption = 'Logradouro:'
          end
          object Label15: TLabel
            Left = 387
            Top = 27
            Width = 41
            Height = 13
            Caption = 'N'#250'mero:'
          end
          object Label16: TLabel
            Left = 311
            Top = 62
            Width = 32
            Height = 13
            Caption = 'Bairro:'
          end
          object Label17: TLabel
            Left = 450
            Top = 100
            Width = 23
            Height = 13
            Caption = 'CEP:'
          end
          object Label18: TLabel
            Left = 27
            Top = 100
            Width = 37
            Height = 13
            Caption = 'Cidade:'
          end
          object Label19: TLabel
            Left = 250
            Top = 100
            Width = 37
            Height = 13
            Caption = 'Estado:'
          end
          object Label20: TLabel
            Left = 27
            Top = 62
            Width = 69
            Height = 13
            Caption = 'Complemento:'
          end
          object EdtLog1: TEdit
            Left = 92
            Top = 24
            Width = 260
            Height = 21
            TabOrder = 0
          end
          object EdtNumero1: TEdit
            Left = 442
            Top = 24
            Width = 81
            Height = 21
            TabOrder = 1
          end
          object EdtBairro1: TEdit
            Left = 349
            Top = 62
            Width = 151
            Height = 21
            TabOrder = 3
          end
          object EdtCEP1: TMaskEdit
            Left = 479
            Top = 100
            Width = 120
            Height = 21
            EditMask = '00000\-999;1;_'
            MaxLength = 9
            TabOrder = 6
            Text = '     -   '
          end
          object EdtCidade1: TEdit
            Left = 70
            Top = 100
            Width = 121
            Height = 21
            TabOrder = 4
          end
          object CmbEstado1: TComboBox
            Left = 293
            Top = 100
            Width = 94
            Height = 21
            ItemHeight = 13
            TabOrder = 5
            Items.Strings = (
              'AC'
              'AL'
              'AP'
              'AM'
              'BA'
              'CE'
              'DF'
              'ES'
              'GO'
              'MA'
              'MT'
              'MS'
              'MG'
              'PA'
              'PB'
              'PR'
              'PE'
              'PI'
              'RJ'
              'RN'
              'RS'
              'RO'
              'RR'
              'SC'
              'SP'
              'SE'
              'TO')
          end
          object EdtComplemento1: TEdit
            Left = 102
            Top = 62
            Width = 121
            Height = 21
            TabOrder = 2
          end
        end
        object BtnExcluir: TBitBtn
          Left = 497
          Top = 280
          Width = 144
          Height = 25
          Cancel = True
          Caption = 'Exluir'
          TabOrder = 9
          OnClick = BtnExcluirClick
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333FFFFF333333000033333388888833333333333F888888FFF333
            000033338811111188333333338833FFF388FF33000033381119999111833333
            38F338888F338FF30000339119933331111833338F388333383338F300003391
            13333381111833338F8F3333833F38F3000039118333381119118338F38F3338
            33F8F38F000039183333811193918338F8F333833F838F8F0000391833381119
            33918338F8F33833F8338F8F000039183381119333918338F8F3833F83338F8F
            000039183811193333918338F8F833F83333838F000039118111933339118338
            F3833F83333833830000339111193333391833338F33F8333FF838F300003391
            11833338111833338F338FFFF883F83300003339111888811183333338FF3888
            83FF83330000333399111111993333333388FFFFFF8833330000333333999999
            3333333333338888883333330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
        end
        object EdtCPF1: TMaskEdit
          Left = 422
          Top = 237
          Width = 120
          Height = 21
          EditMask = '000\.000\.000\-00;1;_'
          MaxLength = 14
          TabOrder = 6
          Text = '   .   .   -  '
        end
        object EdtDataN1: TMaskEdit
          Left = 484
          Top = 199
          Width = 118
          Height = 21
          EditMask = '!99/99/0000;1;_'
          MaxLength = 10
          TabOrder = 4
          Text = '  /  /    '
        end
      end
    end
  end
end
