unit UUsuarios;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ExtCtrls, ComCtrls, StdCtrls, Buttons,SQLiteTable3,SqlTabGrid,
  UDbMod;

type
  TTUsuarios = class(TForm)
    PageControl1: TPageControl;
    Inserir: TTabSheet;
    Editar: TTabSheet;
    ScrollBox1: TScrollBox;
    Splitter1: TSplitter;
    GridAtualizar: TStringGrid;
    ScrollBox2: TScrollBox;
    Splitter2: TSplitter;
    GridInserir: TStringGrid;
    Label1: TLabel;
    EdtNome: TEdit;
    Label2: TLabel;
    cmbTipo: TComboBox;
    Label3: TLabel;
    EdtSenha: TEdit;
    Label4: TLabel;
    cmbSenha: TComboBox;
    BtnInserir: TBitBtn;
    BtnCancelar: TBitBtn;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdtNome1: TEdit;
    EdtCodigo: TEdit;
    EdtSenha1: TEdit;
    cmbTipo1: TComboBox;
    cmbSenha1: TComboBox;
    BtnAtualizar: TBitBtn;
    BtnCancelar1: TBitBtn;
    BtnExcluir: TBitBtn;
    procedure InserirShow(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure BtnInserirClick(Sender: TObject);
    procedure BtnExcluirClick(Sender: TObject);
    procedure EditarShow(Sender: TObject);
    procedure GridAtualizarSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure BtnAtualizarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TUsuarios: TTUsuarios;

implementation
var
T:TSQLiteUniTable;
SQL:string;

{$R *.dfm}

function Rot13(senha:string):string;
var
   i:integer;
   c:char;
   resultado:string;
begin
   for i := 1 to Length(senha) do begin
       c:=senha[i];
       if(c>='A') and (c<='M') then c:=chr(ord(c)+13)
       else if (c>='N') and (c<='Z') then c:=chr(ord(c)-13);
       resultado:=resultado+c
   end;
   Rot13:=resultado
end;

procedure TTUsuarios.BtnAtualizarClick(Sender: TObject);
var
   senha:string;
begin
   senha:=rot13(UpperCase(EdtSenha1.Text));
   SQL:='UPDATE USUARIO SET TIPO="'+cmbTipo1.Text+'",SENHA="'+senha+'",TROCASENHA="'+cmbSenha1.Text+'"';
   SQL:=SQL+' WHERE UN="'+EdtCodigo.Text+'"';
   Try
     B.ExecSQL(SQL);
     ShowMessage('Usu�rio atualizado com sucesso!');
   Except
     on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
   End;

   SQL:='SELECT UN,NOME,TIPO,TROCASENHA FROM USUARIO'  ;
   T:=B.GetUniTable(SQL);
   ExecTableToGrid(T,GridAtualizar);
   GridAtualizar.FixedRows:=1

end;

procedure TTUsuarios.BtnCancelarClick(Sender: TObject);
begin
   EdtNome.Clear;
   cmbTipo.Text:='';
   EdtSenha.Clear;
   cmbSenha.Text:='NAO';
   EdtCodigo.Clear;
   EdtNome1.Clear;
   cmbTipo1.Text:='';
   EdtSenha1.Clear;
   cmbSenha1.Text:='NAO'
end;

procedure TTUsuarios.BtnExcluirClick(Sender: TObject);
var
  confirma:integer;
begin
  confirma:=MessageDlg('Deseja excluir o registro selecionado?',mtWarning,mbYesNo,0);
  if confirma=6 then begin

    SQL:='DELETE FROM USUARIO WHERE UN="'+EdtCodigo.Text+'"';
    Try
       B.execSQL(SQL);
       ShowMessage('Usu�rio exclu�do com sucesso!');
    Except
       on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
    End;

    SQL:='SELECT UN,NOME,TIPO,TROCASENHA FROM USUARIO'  ;
    T:=B.GetUniTable(SQL);
    ExecTableToGrid(T,GridAtualizar);
    GridAtualizar.FixedRows:=1
  end;

end;

procedure TTUsuarios.BtnInserirClick(Sender: TObject);
var
   senha:string;
begin
   senha:=rot13(UpperCase(EdtSenha.Text));
   SQL:='INSERT INTO USUARIO VALUES(NULL,"'+UpperCase(EdtNome.Text)+'","'+cmbTipo.Text+'","'+senha+'","'+cmbSenha.Text+'")';
   Try
     B.ExecSQL(SQL);
     ShowMessage('Usu�rio cadastrado com sucesso!');
   Except
     on E : Exception do ShowMessage(E.ClassName+' Erro! '+E.Message);
   End;

   SQL:='SELECT NOME,TIPO,TROCASENHA FROM USUARIO';
   T:=B.GetUniTable(SQL);
   ExecTableToGrid(T,GridInserir);
   GridInserir.FixedRows:=1

end;

procedure TTUsuarios.EditarShow(Sender: TObject);
begin
  SQL:='SELECT UN,NOME,TIPO,TROCASENHA FROM USUARIO'  ;
  T:=B.GetUniTable(SQL);
  ExecTableToGrid(T,GridAtualizar);
  GridAtualizar.FixedRows:=1
end;

procedure TTUsuarios.FormShow(Sender: TObject);
begin
   PageControl1.ActivePageIndex:=0;
end;

procedure TTUsuarios.GridAtualizarSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
   if Arow =0 then  exit;
   EdtCodigo.Text:=GridAtualizar.Cells[0,Arow];
   EdtNome1.Text:= GridAtualizar.Cells[1,Arow];
   cmbTipo1.Text:= GridAtualizar.Cells[2,Arow] ;
   cmbSenha1.Text:= GridAtualizar.Cells[3,Arow] ;

   SQL:='SELECT SENHA FROM USUARIO WHERE UN="'+EdtCodigo.Text+'"';
   T:=B.GetUniTable(SQL);
   EdtSenha1.Text:=rot13(T.FieldAsString(0))

end;

procedure TTUsuarios.InserirShow(Sender: TObject);
begin
   SQL:='SELECT NOME,TIPO,TROCASENHA FROM USUARIO';
   T:=B.GetUniTable(SQL);
   ExecTableToGrid(T,GridInserir);
   GridInserir.FixedRows:=1

end;

end.
